<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'My_excel@index');
Route::get('excel', 'My_excel@makeXLSX')->name('excel.download');
Route::get('excel-send', 'My_excel@send')->name('excel.send');

Route::get('dinajpur', 'Data_list\Rangpur\Dinajpur@index');
Route::get('panchagarh', 'Data_list\Rangpur\Panchagarh@index');
Route::get('thakurgaon', 'Data_list\Rangpur\Thakurgaon@index');
Route::get('rangpur', 'Data_list\Rangpur\Rangpur@index');

Route::get('chittagong', 'Data_list\Chittagong\Feni@index');

Route::get('chapainawabganj', 'Data_list\Rajshahi\Chapainawabganj@index');
Route::get('naogaon', 'Data_list\Rajshahi\Naogaon@index');

Route::get('satkhira', 'Data_list\Khulna\Satkhira@index');
Route::get('jhenaidah', 'Data_list\Khulna\Jhenaidah@index');

Route::get('pirojpur', 'Data_list\Barisal\Pirojpur@index');

