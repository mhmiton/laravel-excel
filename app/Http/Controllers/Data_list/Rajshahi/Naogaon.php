<?php

namespace App\Http\Controllers\Data_list\Rajshahi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\My_model;

class Naogaon extends Controller
{
   public function index()
   {
   		
		$data = [
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "আত্রাই", "name" => "মোঃ আঃ বারিক", "email" => "biddut.uisc@gmail.com", "phone" => "০১৭১৭-৯০৫২৬২"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "আত্রাই", "name" => "মোছাঃ সালমা বেগম", "email" => "salma.uisc@gmail.com", "phone" => "০১৯৪৬-৪৯৪১৭৮"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "আত্রাই", "name" => "মোঃ শফিকুল ইসলাম", "email" => "bpara.uisc@gmail.com", "phone" => "০১৭১৩-৭৯৩৫২৫"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "আত্রাই", "name" => "মোছাঃ কামরম্নন নাহার", "email" => "uisc.bpara@gmail.com", "phone" => "০১৭৩৮-১৬০১৫৯"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "আত্রাই", "name" => "এরশাদ আলী", "email" => "masud716@gmail.com", "phone" => "০১৭১৭-৬২২৯৭৫"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "আত্রাই", "name" => "রবি রায়হান", "email" => "roby.nissan707@gmail.com", "phone" => "০১৮২৭-৪৪০৩৬৭"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "আত্রাই", "name" => "শারমিন", "email" => "sarmin.uisc@yahoo.com", "phone" => "০১৮৩৬-৭০১৪৮৮"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "আত্রাই", "name" => "এম এ মতিন", "email" => "motin.uisc@gmail.com", "phone" => "০১৭১৬-৫৮৪৩৭৬"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "আত্রাই", "name" => "আরজিন বানু", "email" => "banuarzina@yahoo.com", "phone" => "০১৭৩৭-৬৮৩৮১৮"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "আত্রাই", "name" => "মোঃ আসলাম হোসেন মৃধা", "email" => "aslam.uisc@yahoo.com", "phone" => "০১৭১৫-১৭০৪৭১"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "আত্রাই", "name" => "নাজমা খাতুন", "email" => "nazma.uisc@yahoo.com", "phone" => "০১৭৪৯-৪৬১২৬৭"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "আত্রাই", "name" => "মোছাঃ সানজিদা আলম", "email" => "ah.uisc@yahoo.com", "phone" => "০১৭৫৮-১৩৬১৮৩"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "আত্রাই", "name" => "আকরাম হোসেন", "email" => "ah.apon@yahoo.com", "phone" => "০১৭৩৭-৭০৬৬৮১"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "আত্রাই", "name" => "সেলিম হোসেন", "email" => "uisc.bisha2011@yahoo.com", "phone" => "০১৭১৮-৫৬৩১৫২"],

			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "রাণীনগর", "name" => "মোছা: মাহবুবা জামানী", "email" => "ety_zamany@yahoo.com", "phone" => "০১৭৮৪-১২২৫২৭"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "রাণীনগর", "name" => "মোঃ আব্দুল আওয়াল", "email" => "auwal_hakim1988@yahoo.com", "phone" => "০১৭৪৯-৭৬৯৩৩২"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "রাণীনগর", "name" => "মোঃ শরিফুল ইসলাম", "email" => "shoriful61@yahoo.com", "phone" => "০১৭২৩-৯৮০৭৯৯"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "রাণীনগর", "name" => "রিভা পারভীন", "email" => "khatun_riva@yahoo.com", "phone" => "০১৭২৩-৫৮০৩৮৫"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "রাণীনগর", "name" => "মোঃ হাফিজুর রহমান", "email" => "hafizur0170@yahoo.com", "phone" => "০১৭৪০-৮০৮২৬০"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "রাণীনগর", "name" => "মোছাঃ খাদিজা আকতার", "email" => "khadiza0170@yahoo.com", "phone" => "০১৭৫৫-২৭২১২৫"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "রাণীনগর", "name" => "মোঃ মোসত্মফা কামাল", "email" => "mostafauisc@gmail.com", "phone" => "০১৭৪৬-৩২৫৬৮২"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "রাণীনগর", "name" => "উত্তম কুমার সাহা", "email" => "uttumuisc@gmail.com", "phone" => "০১৭২৯-৩১৪২৭৫"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "রাণীনগর", "name" => "মোঃ জাহাঙ্গীর আলম", "email" => "jahangir.uisc@gmail.com", "phone" => "০১৭১৬-৬০৯৯৬২"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "রাণীনগর", "name" => "ফাহিমা খাতুন", "email" => "uisc.mfk02@gmail.com", "phone" => "০১৭১১-৭১৭১১৭"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "রাণীনগর", "name" => "মোঃ রেজাউল ইসলাম", "email" => "rezaul_976@yahoo.com", "phone" => "০১৭১৬-৩৪৬৯৭৬"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "রাণীনগর", "name" => "মোছাঃ আজিফা খানম", "email" => "uisc.mak04@gmail.com", "phone" => "০১৭৪৪-৪৮৩৭৬২"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "রাণীনগর", "name" => "মোঃ ফিরোজ মাহমুদ", "email" => "uisc.mfm05@gmail.com", "phone" => "০১৭৪০-৬৪৮৩১৮"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "রাণীনগর", "name" => "মোছাঃ তানজিলা", "email" => "uisc.mfm06@yahoo.com", "phone" => "০১৭৩৬-৭৩৩৩৯৩"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "রাণীনগর", "name" => "মোঃ রম্নহুল আমিন", "email" => "tanvirrafi05@yahoo.com", "phone" => "০১৭৪০-৫৭৮৭২৫"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "রাণীনগর", "name" => "মোছাঃ ফারহানা বেগম", "email" => "farhana.uisc@gmail.com", "phone" => "০১৭১৮-৮৪২৬০০"],

			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "পত্নীতলা", "name" => "মোঃ আমিমুল ইহসান", "email" => "babu.uiscpu@yahoo.com", "phone" => "০১৭১২-৬৫৪৮১২"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "পত্নীতলা", "name" => "লিপিকা লিন্ডা", "email" => "lipika.uiscpu@gmail.com", "phone" => "০১৭৬৭-২৪৯৭৮৬"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "পত্নীতলা", "name" => "মো: সোহরাব হোসেন", "email" => "sohel.rupgram@gmail.com", "phone" => "০১৭৬১-৫২৭৫৭৪"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "পত্নীতলা", "name" => "মোছাঃ নাছিমা খাতুন", "email" => "nasima.01756@gmail.com", "phone" => "০১৭৫৬-৮৬৬৪৯১"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "পত্নীতলা", "name" => "খোরশেদুল আলম রানা", "email" => "nijhum_rana@yahoo.com", "phone" => "০১৭১১-৪১২৫০৭"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "পত্নীতলা", "name" => "মেসকাতুল জান্নাত", "email" => "khorsadul@gmail.com", "phone" => "০১৭১১-৪১২২২০"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "পত্নীতলা", "name" => "মোঃ হামিম", "email" => "hamimrelly@gmail.com", "phone" => "০১৭৩৫-০৮৪০৫৮"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "পত্নীতলা", "name" => "মোঃ আহসান হাবীব", "email" => "ahshanhabib1985@gmail.com", "phone" => "০১৭৩৬-৬৭০৯৮৯"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "পত্নীতলা", "name" => "মোঃ নূরে আলম সিদ্দিক", "email" => "nurealam.nosin@gmail.com", "phone" => "০১৭৩২-১২১৩০৭"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "পত্নীতলা", "name" => "এটিএম আশেকুল ইসলাম", "email" => "ashekulraihan167@gmail.com", "phone" => "০১৭৩৭-২৮৭১৬৭"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "পত্নীতলা", "name" => "প্রণয় কুমার মহমত্ম", "email" => "pronymohonto@gmail.com", "phone" => "০১৭৩৭-৬০০০০৮"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "পত্নীতলা", "name" => "মোঃ তানজির তানিন", "email" => "tanzirtanin@gmail.com", "phone" => "০১৭৪৫-৩১৭৫৭৫"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "পত্নীতলা", "name" => "দেলোয়ার হোসেন উজ্জল", "email" => "uzzal.01724@gmail.com", "phone" => "০১৭২৪-৭৭৭৭৫৯"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "পত্নীতলা", "name" => "আবু ফিরোজ", "email" => "firozhossain83@gmail.com", "phone" => "০১৭৩৩-১৮৪০৭৫"],

			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "ধামইরহাট", "name" => "সুলতান মাহমুদ", "email" => "sutanuisc@gmail.com", "phone" => "০১৮২১-৯৫২৩৩৩"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "ধামইরহাট", "name" => "মোছাঃ ফাতেমা", "email" => "sutanuisc@gmail.com", "phone" => "০১৭২৫-৮৫৪৯৩২"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "ধামইরহাট", "name" => "যোবায়ের হোসাইন", "email" => "jobairhossin851@gmail.com", "phone" => "০১৭৩৬-৫৯৫৯২৩"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "ধামইরহাট", "name" => "মোছাঃ রাবেয়া সুলতানা", "email" => "rabuiyasultana1990@gmail.com", "phone" => "০১৯৪৬-৪৯৪১৭২"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "ধামইরহাট", "name" => "রিপন হোসেন", "email" => "mdriponhossain875@yahoo.com", "phone" => "০১৭৪০-০৩২৮৭২"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "ধামইরহাট", "name" => "মোছাঃ ফারজানা", "email" => "mdriponhossain875@yahoo.com", "phone" => "০১৭৪৭-৬৭৮৬৮৮"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "ধামইরহাট", "name" => "আসাদুজ্জামান", "email" => "ashad_bnd@yahoo.com", "phone" => "০১৭১৭-৫১৬৬৬১"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "ধামইরহাট", "name" => "মোছাঃ শিউলী বেগম", "email" => "uisc.sb11@gmail.com", "phone" => "০১৭২৩-৮১৮৫৯৮"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "ধামইরহাট", "name" => "আঃ রহিম", "email" => "uisc.mar325@yahoo.com", "phone" => "০১৭১৪-৫১২০৫৯"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "ধামইরহাট", "name" => "মোছাঃ শারমিন আক্তার", "email" => "uisc.mar325@yahoo.com", "phone" => "০১৭৬৬-০৪৭৬৫৫"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "ধামইরহাট", "name" => "আনিছুর রহমান", "email" => "uisc.sk12@gmail.com", "phone" => "০১৭১৩-৭০৫৮৭৬"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "ধামইরহাট", "name" => "মোছাঃ শিরিনা", "email" => "uisc.sk12@gmail.com", "phone" => "০১৭৩৬-২৯২৮১৯"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "ধামইরহাট", "name" => "আঃ জলিল", "email" => "uisc.maj131@gmail.com", "phone" => "০১৮২৬-১৯২১৯৩"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "ধামইরহাট", "name" => "মোছাঃ ছাবরিনা", "email" => "uisc.sb132@gmail.com", "phone" => "০১৭১৬-৩৩৪১৪২"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "ধামইরহাট", "name" => "সুশামত্ম কুমার", "email" => "uisc.sks231@gmail.com", "phone" => "০১৭১৯-৪৬৭৭০৯"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "ধামইরহাট", "name" => "প্রীতি রাণী সাহা", "email" => "uisc.prs10@gmail.com", "phone" => "০১৭৬২-৭১৬৬৬৭"],

			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "বদলগাছী", "name" => "মোঃ আবু তাহের সিফাত", "email" => "badalgachi.uic@gmail.com", "phone" => "০১৭১৮-১৮৫২৪৩"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "বদলগাছী", "name" => "মোছাঃ শারমিন সুলতানা (আঁখি)", "email" => "badalgachi.uic@gmail.com", "phone" => "০১১৯৭-৪০২৬৯৪"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "বদলগাছী", "name" => "মোঃ নাজমুল হোসেন", "email" => "uisc.mzs319@yahoo.com", "phone" => "০১৯১২-১১০২৯৯"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "বদলগাছী", "name" => "মোছাঃ জাকিযা সুলতানা", "email" => "uisc.mzs319@yahoo.com", "phone" => "০১৭১৭-৯৯৮৩৩১"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "বদলগাছী", "name" => "মোঃ দুলাল হোসেন", "email" => "dulaleti99@gmail.com", "phone" => "০১৭৫৫-৪৪৮০৫৫"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "বদলগাছী", "name" => "জেড. জাকিয়া", "email" => "uiscuddogta_1994@yahoo.com", "phone" => "০১৯৩৩-৪০৯৪৪২"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "বদলগাছী", "name" => "তানজিলা আকতার লিজা", "email" => "uisc04@gmail.com", "phone" => "০১৭৩৫-৯০৭৬৫০"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "বদলগাছী", "name" => "শরিফুল ইসলাম", "email" => "mdsharifulislam876@gmail.com", "phone" => "০১৭৪৯-৬০০০৬৮"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "বদলগাছী", "name" => "মোছাঃ রাজিয়া সুলতানা", "email" => "razia850@gmail.com", "phone" => "০১৭৩৯-২৮৯৭৮৭"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "বদলগাছী", "name" => "মোঃ আলমগীর হোসেন", "email" => "alamgirhossain852@yahoo.com", "phone" => "০১৭২৯-৬৯০৮৫২"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "বদলগাছী", "name" => "মোছাঃ মর্জিনা বানু", "email" => "uisc.marjina@gmail.com", "phone" => "০১৭৭৪-৬১১১৪৪"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "বদলগাছী", "name" => "পার্থ চন্দ্র হাজরা", "email" => "lipikarani99@yahoo.com", "phone" => "০১৭১২-৪৭৩৮৫৬"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "বদলগাছী", "name" => "লিপিকা রাণী মন্ডল", "email" => "lipikarani99@yahoo.com", "phone" => "০১৫৫৭-৮৬৯৬৬২"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "বদলগাছী", "name" => "মোঃ বেলাল হোসেন", "email" => "uisc.belalhossain@gmail.com", "phone" => "০১৭১২-৪৬৭৭০৯"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "বদলগাছী", "name" => "মোসাঃ রোকসানা আকতার", "email" => "uisc.belalhossain@gmail.com", "phone" => "০১৭২২-৯০৩৫৬৬"],

			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "মান্দা", "name" => "রনজিত কুমার", "email" => "uisc.rk11@gmail.com", "phone" => "০১৭১৩-৭৪৪৭২৯"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "মান্দা", "name" => "শামত্মনা রানী", "email" => "uisc.sr11@gmail.com", "phone" => "০১৭১৩-৭৮৪৩৮১"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "মান্দা", "name" => "ফেন্সি আক্তার", "email" => "fensi566@yahoo.com", "phone" => "০১৭৩৬-২৯৬২৩৬"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "মান্দা", "name" => "মিলন কুমার মন্ডল", "email" => "milon01717927449@gmail.com", "phone" => "০১৭১৭-৯২৭৪৪৯"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "মান্দা", "name" => "মোছাঃ রাফিয়া খাতুন", "email" => "rafia01738@ gmail.com", "phone" => "০১৭৩৮-৪৭৬০১৭"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "মান্দা", "name" => "মোঃ শাকিব হাসান", "email" => "sakib.manda@yahoo.com", "phone" => "০১৯৩৭-৩৬১৫৯৩"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "মান্দা", "name" => "মোছাঃ ফেনসি আক্তার", "email" => "fanciakter45656@yahoo.com", "phone" => "০১৭২৩-৭৬৪৩৭৪"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "মান্দা", "name" => "মোছাঃ শারমিনা আক্তার", "email" => "sarminakter555@yahoo.com", "phone" => "০১৭৬৪-৬৩৮১১৭"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "মান্দা", "name" => "মোঃ আব্দুল মতিন", "email" => "uisc.abdulmatitin@gmail.com", "phone" => "০১৭৩৪-০০৮৩৪৯"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "মান্দা", "name" => "সামসুন নাহার", "email" => "samsunnahar.uisc@gmail.com", "phone" => "০১৭২৫-৪৫৬৮২২"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "মান্দা", "name" => "মোঃ মোখলেছুর রহমান", "email" => "moklesur.uisc@gmail.com", "phone" => "০১৭২৮-৩২২৯৬৩"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "মান্দা", "name" => "মোছাঃ নাদিরা খাতুন", "email" => "nadira1234@yahoo.com", "phone" => "০১৯১৮-৮৯০১৪৩"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "মান্দা", "name" => "মোঃ জহুরম্নল ইসলাম", "email" => "uisc.prosadpur@yahoo.com", "phone" => "০১৯৪৬-৪৯৪১৯৮"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "মান্দা", "name" => "মোছাঃ ফারজানা আক্তার", "email" => "farzanamanda@yahoo.com", "phone" => "০১৭২৮-০৩০৭০২"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "মান্দা", "name" => "মোঃ আফতাব হোসেন", "email" => "aftab. uisc@ gmail.com", "phone" => "০১৭১০-৬৪৯৮৭৫"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "মান্দা", "name" => "মোছাঃ মাহমুদা আক্তার", "email" => "mahamuda.akter@yahoo.com", "phone" => "০১৭২১-৫১২৭৯৮"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "মান্দা", "name" => "মোঃ আরিফুল ইসলাম", "email" => "uisc.kashob@yahoo.com", "phone" => "০১৭৫১-৭২২৬৫৪"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "মান্দা", "name" => "মোসাঃ রম্নকসানা আক্তার স্বর্ণা", "email" => "ruksanaakter45@yahoo.com", "phone" => "০১৭৩২-১১৮০০১"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "মান্দা", "name" => "মোঃ এনামুল হোসেন মৃধা", "email" => "uisc.mah51@gmail.com", "phone" => "০১৭২৪-৩৪০৪৫১"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "মান্দা", "name" => "সাহানারা বেগম", "email" => "uisc.mah51@gmail.com", "phone" => "০১৯৩৮-৩৬৭৩২৬"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "মান্দা", "name" => "মোঃ সিরাজুল ইসলাম", "email" => "uisc.valain@yahoo.com", "phone" => "০১৭৩৬-১৫৬৬৭২"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "মান্দা", "name" => "মোঃ রম্নহুল আমিন", "email" => "ruhulamin767@yahoo.com", "phone" => "০১৯৩৭-৪৯০১৮৮"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "মান্দা", "name" => "মোছাঃ নূর নাহার", "email" => "nurnahar_156@gmail.com", "phone" => "০১৯২৭-২৭৪৮৭১"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "মান্দা", "name" => "মো: আরিফুল ইসলাম", "email" => "uisckalikapur@yahoo.com", "phone" => "০১৭৫০-২৯২৯৮৩"],

			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "নওগাঁ সদর", "name" => "মোঃ রোকনুজ্জামান", "email" => "uisc.mr1@gmail.com", "phone" => "০১৭১৮-৬৬৭২৫২"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "নওগাঁ সদর", "name" => "মোছাঃ হাফিজা বানু", "email" => "uisc.hafija123@gmail.com", "phone" => "০১৭৪৩-৯৭২৩০৫"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "নওগাঁ সদর", "name" => "মোঃ রহিদুল ইসলাম", "email" => "rahidulislam179@gmail.com", "phone" => "০১৭৩৪-৩২৪৫২৪"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "নওগাঁ সদর", "name" => "মোঃ শাফিউর রাফাত", "email" => "rokon508@gmail.com", "phone" => "০১৭৩৭-৫৯৯৩৩৬"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "নওগাঁ সদর", "name" => "মোঃ ফরহাত হোসেন", "email" => "farhadhassan010@gmail.com", "phone" => "০১৭৫৫-১৪৭৯৪১"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "নওগাঁ সদর", "name" => "মোঃ কাজল হোসেন", "email" => "farhadhassan010@yahoo.com", "phone" => "০১৭৫৩-০৬২১৯৩"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "নওগাঁ সদর", "name" => "মোঃ আব্দুল মোতালেব হোসেন", "email" => "uisc.mamh09@gmail.com", "phone" => "০১৭৩৮-৬৮৮১৫১"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "নওগাঁ সদর", "name" => "মারিফুল ইসলাম", "email" => "jalalhossain457@gmail.com", "phone" => "০১৭৫০-৩৩৪৭৮৭"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "নওগাঁ সদর", "name" => "মোঃ আল মাহমুদ", "email" => "uisc.mahmud@gmail.com", "phone" => "০১৯১৭-৩২২২৯৯"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "নওগাঁ সদর", "name" => "মোঃ হাসানুজ্জামান সবুজ", "email" => "hasanuzzamansobuz@gmail.com", "phone" => "০১৭৫৪-৬০৯৬৭২"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "নওগাঁ সদর", "name" => "মোঃ আজিজুল হক", "email" => "azizraton@gmail.com", "phone" => "০১৯৪৭-২৬৩৪৪৬"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "নওগাঁ সদর", "name" => "দেওয়ান রাফিউল ইসলাম", "email" => "rafi6500@gmail.com", "phone" => "০১৭৩৭-২৯৯৫৭৪"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "নওগাঁ সদর", "name" => "মোঃ সেলিম রেজা", "email" => "uisc.msr019@yahoo.com", "phone" => "০১৭৩৫-৯৫৮৪৫৭"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "নওগাঁ সদর", "name" => "মোঃ আবু বকর সিদ্দিক", "email" => "Bokkorrojoni@yahoo.com", "phone" => "০১৭৩৩-১৯৭২০১"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "নওগাঁ সদর", "name" => "মোঃ মাসুদ রানা", "email" => "uisc.mmr06@gmail.com", "phone" => "০১৭১১-৪১৩৪৪০"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "নওগাঁ সদর", "name" => "মোঃ আসলাম হোসেন", "email" => "uisc.mah@gmail.com", "phone" => "০১৭৪০-৫১৫৩২০"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "নওগাঁ সদর", "name" => "মোঃ আব্দুল হান্নান", "email" => "uisc.mah02@gmail.com", "phone" => "০১৭১৫-৪৮৪৭৩২"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "নওগাঁ সদর", "name" => "মোঃ নার্গিস বানু", "email" => "nargis.tilakpurptotho@yahoo.com", "phone" => "০১৭৫৮-০৬৯৯১৫"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "নওগাঁ সদর", "name" => "মো: আলামিন", "email" => "alaminuisc251@gmail.com", "phone" => "০১৭৬৭-৪২৩১৬৪"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "নওগাঁ সদর", "name" => "মো: আরিফ হোসেন", "email" => "kritipurbd@gmail.com", "phone" => "০১৭৬৩-৭২২৮২৭"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "নওগাঁ সদর", "name" => "মোঃ এবাইদুল", "email" => "uisc.abaidul017@gmail.com", "phone" => "০১৭৫২-৪০১৬২৬"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "নওগাঁ সদর", "name" => "মোঃ সিরাজুল ইসলাম", "email" => "sirajulislam124@yahoo.com", "phone" => "০১৭২৩-২৪৮২৭৬"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "নওগাঁ সদর", "name" => "মোঃ এনামুল হক", "email" => "uisc.eho7@gmail.com", "phone" => "০১৭৪৫-৩৫৬৭৬৬"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "নওগাঁ সদর", "name" => "মোঃ বিপস্নব হোসেন", "email" => "bhossen74@gmail.com", "phone" => "০১৭৩৮-৪২০৭৫৬"],

			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "মহাদেবপুর", "name" => "মোঃ রেজানুর রহমান", "email" => "manikbce@yahoo.com", "phone" => "০১৭১২-৯৫০৮৫৬"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "মহাদেবপুর", "name" => "মোঃ আশরাফুল আলম", "email" => "md.shohel73@yahoo.com", "phone" => "০১৭২৯-৯১১৯১৯"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "মহাদেবপুর", "name" => "সাদ্দাম হোসেন", "email" => "saddumuttokta@gmail.com", "phone" => "০১৭৪৭-০৩৮৮৭৬"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "মহাদেবপুর", "name" => "মোঃ আরিফুল ইসলাম", "email" => "uiscarif10@gmail.com", "phone" => "০১৭৩৪-৪৪৪২১২"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "মহাদেবপুর", "name" => "ইকবাল হাসান", "email" => "uisc4.iqbal.hasan@gmail.com", "phone" => "০১৭১৮-৭৭৯৮১০"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "মহাদেবপুর", "name" => "জাহিদুল ইসলাম", "email" => "jahidulislamjahid44@gmail.com", "phone" => "০১৭২৩-৮৪০৮১৯"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "মহাদেবপুর", "name" => "দিলশাদ বেগম", "email" => "dilshad.uisc@gmail.com", "phone" => "০১৮৪৫-৬১৮০৯২"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "মহাদেবপুর", "name" => "মোঃ দেলোয়ার হোসেন", "email" => "uisc.mdh05@gmail.com", "phone" => "০১৭২৮-৭১৯৬৫১"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "মহাদেবপুর", "name" => "মোঃ হারম্নন অর রশিদ", "email" => "harun.uisc@gmail.com", "phone" => "০১৭৫৩-০২৬৭৩৬"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "মহাদেবপুর", "name" => "মো: মাহবুব আলম", "email" => "mdmahabub69@yahoo.com", "phone" => "০১৭৩৯-৫৬৭৭৯০"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "মহাদেবপুর", "name" => "মোঃ হযরত আলী", "email" => "uischazratali@yahoo.com", "phone" => "০১৭৫৪-৬১১২৩৮"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "মহাদেবপুর", "name" => "রাবেয়া রহমান", "email" => "rabeya.123.uisc@gmail.com", "phone" => "০১৭৩৩-১৯৭৬৫০"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "মহাদেবপুর", "name" => "মোঃ লুৎফর রহমান", "email" => "lutfar.uisc@gmail.com", "phone" => "০১৭১৩-৭৮০২৯৫"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "মহাদেবপুর", "name" => "মোঃ হাফিজুর রহমান", "email" => "hafizur4400@gmail.com", "phone" => "০১৭২৩-৭৬৪৪০০"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "মহাদেবপুর", "name" => "মোঃ টিপু সুলতান", "email" => "tipu.sultan708@gmail.com", "phone" => "০১৭১৩-৭৮৪৪৪৯"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "মহাদেবপুর", "name" => "মোঃ আব্দুর রাজ্জাক", "email" => "vimpurup.naogaon@gmail.com", "phone" => "০১৭২৩-১১০৮০২"],

			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "সাপাহার", "name" => "মোঃ ফিরোজ হোসেন", "email" => "firozhossain93@yahoo.com", "phone" => "০১৭১০-০৬১৯২৯"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "সাপাহার", "name" => "মোছাঃ নাছিমা খাতুন", "email" => "resmijahan1989@gmail.com", "phone" => "০১৭৫৯-০২১২১৪"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "সাপাহার", "name" => "মোঃ মহরম", "email" => "uisc.mmao3@gmail.com", "phone" => "০১৭৪০-৮০৭৬০২"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "সাপাহার", "name" => "মো: রমজান আলী", "email" => "uisc.mmao3@gmail.com", "phone" => "০১৭৪০-৮৬৪২৫৯"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "সাপাহার", "name" => "মোঃ মাইনুর রহমান", "email" => "uisc.myunur.ahaman@gmail.com", "phone" => "০১৭২৪-৯৫৮৩৬৩"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "সাপাহার", "name" => "মোছাঃ লতিফা খাতুন", "email" => "uisc.myunur.ahaman@gmail.com", "phone" => "০১৭৫৮-৪৪৯২৭০"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "সাপাহার", "name" => "মোঃ আব্দুর রউফ", "email" => "r877955@gmail.com", "phone" => "০১৭২৯-৮৭৭৯৫৫"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "সাপাহার", "name" => "মোঃ রিযাজুল ইসলাম", "email" => "r877955@gmail.com", "phone" => "০১৭১৯-০৩৫৪০৩"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "সাপাহার", "name" => "আঃ জববার", "email" => "uisc.ma303@gmail.com", "phone" => "০১৭২৫-৫৯১৮৬৪"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "সাপাহার", "name" => "মোঃ ইমাম হোসেন", "email" => "uisc.ma303@gmail.com", "phone" => "০১৭১৩-৭১৮৩৪৯"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "সাপাহার", "name" => "মোঃ নজরম্নল", "email" => "uisc.nazrul.pra@gmail.com", "phone" => "০১৭২৯-৭৩৪২৫৮"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "সাপাহার", "name" => "মোঃ নজমুল", "email" => "uisc.nazrul.pra@gmail.com", "phone" => "০১৭৩৬-৫৩০৯৫৬"],

			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "নিয়ামতপুর", "name" => "মোঃ মিনহাজুল ইসলাম", "email" => "minhajuisc@gmail.com", "phone" => "০১৭৩৭-২৪১৮৮৩"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "নিয়ামতপুর", "name" => "মোছাঃ মনিরা আক্তার", "email" => "monira.uisc2@gmail.com", "phone" => "০১৯৩৭-২৫৭৮০৯"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "নিয়ামতপুর", "name" => "পরিমল", "email" => "parimailbormon36@yahoo.com", "phone" => "০১৭১৪-৮৬৪৮৮৫"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "নিয়ামতপুর", "name" => "মোছাঃ খাদিজা খাতুন (মনি)", "email" => "uisc.mk.021@yahoo.com", "phone" => "০১৭৪০-৩৮৩১৩১"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "নিয়ামতপুর", "name" => "মোছাঃ জাকিয়া খাতুন", "email" => "mstjakiakhatun@gmail.com", "phone" => "০১৮২৯-৩০৮৮৬০"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "নিয়ামতপুর", "name" => "মোঃ আঃ মান্নান", "email" => "mannan.bhabicha @yahoo.com", "phone" => "০১৭১৫-৬৫১৮৩৮"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "নিয়ামতপুর", "name" => "মোঃ সেলিম রেজা", "email" => "selimf50@yahoo.com", "phone" => "০১৭২৮-১৭২৩৫০"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "নিয়ামতপুর", "name" => "মোছাঃ সাহেরা খাতুন", "email" => "uisc.msk310@yahoo.com", "phone" => "০১৭১৩-২১৯৮৬২"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "নিয়ামতপুর", "name" => "মোঃ শহিদুল ইসলাম", "email" => "uzzall.bd@gmail.com", "phone" => "০১৭২৪-৬৬৯২৪৭"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "নিয়ামতপুর", "name" => "রিপা রাণী", "email" => "uisc.md@gmail.com", "phone" => "০১৭৪৮-৭৪৮২৮২"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "নিয়ামতপুর", "name" => "প্রভাত চন্দ্র", "email" => "provassbd@yahoo.com", "phone" => "০১৭৪৩-৬৯৩০৬২"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "নিয়ামতপুর", "name" => "মোছাঃ সাজেনুর খাতুন", "email" => "msajenur@yahoo.com", "phone" => "০১৭৬৩-০৭৭৭২০"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "নিয়ামতপুর", "name" => "মোঃ শাহীন আক্তার", "email" => "shaineakther@gmail.com", "phone" => "০১৭৩৪-৩৫০২১৬"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "নিয়ামতপুর", "name" => "মোসাঃ ফরিদা বেগম", "email" => "mstfarida.sreemantapur@gmail.com", "phone" => "০১৭৪৯-০১৯৬৩৫"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "নিয়ামতপুর", "name" => "মোঃ তরিকুল ইসলাম টুটুল", "email" => "ti.tutul8@yahoo.com", "phone" => "০১৭২৩-৭১৭৭১২"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "নিয়ামতপুর", "name" => "মোসাঃ মারম্নপা খাতুন", "email" => "k.marufa@yahoo.com", "phone" => "০১৭৫০-৩২৯৪৬৩"],

			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "পোরশা", "name" => "মোসাঃ জোসনা খাতুন", "email" => "uisc.mjk11@gmail.com", "phone" => "০১৭২৩-৫১৯৪৪৬"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "পোরশা", "name" => "মোঃ শাহজামান হোসেন", "email" => "uisc.msz02@gmail.com", "phone" => "০১৭১৭-১৪৩২৫০"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "পোরশা", "name" => "মোঃ আইনুল ইসলাম রানা", "email" => "mai202001@gmail.com", "phone" => "০১৭২৪-৪৮৪৩৯৭"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "পোরশা", "name" => "মোসাঃ নুরেজা খাতুন", "email" => "uisc.mnk06@gmail.com", "phone" => "০১৭৫১-২৫৪৭৬২"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "পোরশা", "name" => "মোঃ বেলাল হোসেন", "email" => "m.belal.67@gmail.com", "phone" => "০১৭৩৮-৪২১৫৬৯"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "পোরশা", "name" => "মোঃ এনামুল হক", "email" => "md.enamulhaque41@yahoo.com", "phone" => "০১৭১৯-১০৬৪১৭"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "পোরশা", "name" => "মোঃ আকতারম্নজ্জামান", "email" => "aktarpc@gmail.com", "phone" => "০১৭৩৪-৬২৯৮৮৩"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "পোরশা", "name" => "মোছাঃ মোশরেফা খাতুন", "email" => "mosrefau@gmail.com", "phone" => "০১৭৬৪-৮২৬৮৬০"],
			["division" => "রাজশাহী", "dist" => "নওগাঁ", "sub_dist" => "পোরশা", "name" => "মোঃ সাকিরম্নল ইসলাম", "email" => "shakirul1771@gmail.com", "phone" => "০১৭২৯-৩৮৯৯৮৪"]
		];

		$trans	= ["১"=>"1", "২"=>"2", "৩"=>"3", "৪"=>"4", "৫"=>"5", "৬"=>"6", "৭"=>"7", "৮"=>"8", "৯"=>"9", "০"=>"0", "o"=>"0", "O"=>"0"];

		for ($i=0; $i < count($data); $i++) {
			$list['division'] 		= $data[$i]['division'];
			$list['dist'] 			= $data[$i]['dist'];
			$list['sub_dist'] 		= $data[$i]['sub_dist'];
			$list['name'] 			= $data[$i]['name'];
			$list['phone'] 			= strtr($data[$i]['phone'], $trans);
			$list['email'] 			= $data[$i]['email'];
			$list['created_at'] 	= \Carbon\Carbon::now();

			//My_model::insert('entrepreneurs_list', $list);
		}
			return "It's Completed";
   }
}