<?php

namespace App\Http\Controllers\Data_list\Rajshahi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\My_model;

class Chapainawabganj extends Controller
{
   public function index()
   {
   		
		$data = [
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "চাঁপাইনবাবগঞ্জসদর", "name" => "মো: ঈমান আলী", "email" => "iemanali2013@gmail.com", "phone" => "01773021663"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "চাঁপাইনবাবগঞ্জসদর", "name" => "মোসা: নুরনাহার", "email" => "nurnahar80@gmail.com", "phone" => "01727554675"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "চাঁপাইনবাবগঞ্জসদর", "name" => "কল্লোল সরকার (জয়)", "email" => "joykollol@yahoo.com", "phone" => "01720595189"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "চাঁপাইনবাবগঞ্জসদর", "name" => "মোসা: সাবিনা খাতুন", "email" => "joysarkar2010@gmail.com", "phone" => "01772856685"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "চাঁপাইনবাবগঞ্জসদর", "name" => "মো: ওয়ালিদ হাসান", "email" => "walidhasan1981@gmail.com", "phone" => "01716123681"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "চাঁপাইনবাবগঞ্জসদর", "name" => "মোসা: লিলি খাতুন", "email" => "lilykhatun1984@gmail.com", "phone" => "01920358020"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "চাঁপাইনবাবগঞ্জসদর", "name" => "মো: মেহেদী হাসান", "email" => "nce@amardesh.com", "phone" => "01746699199"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "চাঁপাইনবাবগঞ্জসদর", "name" => "মোসা: আন্জুমান আরা", "email" => "anjumanarakhatun002@gmail.com", "phone" => "01741189616"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "চাঁপাইনবাবগঞ্জসদর", "name" => "মো: আজমুল হাসান", "email" => "azmulhasan@yahoo.com", "phone" => "01713294040"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "চাঁপাইনবাবগঞ্জসদর", "name" => "মোসা: মেহেরুন নেসা", "email" => "meherunnesa4040@yahoo.com", "phone" => "01796059569"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "চাঁপাইনবাবগঞ্জসদর", "name" => "মো: মোস্তাফিজুর রহমান", "email" => "m.rase118@gmail.com", "phone" => "01728306986"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "চাঁপাইনবাবগঞ্জসদর", "name" => "সুলতানা রাজিয়া", "email" => "m.rase118@gmail.com", "phone" => "01925719595"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "চাঁপাইনবাবগঞ্জসদর", "name" => "মো: ইউসুফ আলী", "email" => "yusuf01747@gmail.com", "phone" => "01747001139"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "চাঁপাইনবাবগঞ্জসদর", "name" => "মোসা: মাহমুদা খাতুন", "email" => "m.khatun85@yahoo.com", "phone" => "01723786778"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "চাঁপাইনবাবগঞ্জসদর", "name" => "মো: মোবারক হোসেন", "email" => "islampur.hosin@gmail.com", "phone" => "01728968836"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "চাঁপাইনবাবগঞ্জসদর", "name" => "জিয়াসমিন খাতুন", "email" => "kziasmin@yahoo.com", "phone" => "01743330150"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "চাঁপাইনবাবগঞ্জসদর", "name" => "মো: নাসিম আখতার (কাফি)", "email" => "md.nasimakhtarkafi@yahoo.com", "phone" => "01757844732"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "চাঁপাইনবাবগঞ্জসদর", "name" => "আসমাউলহুসনা", "email" => "asmultoma@yahoo.com", "phone" => "01732689085"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "চাঁপাইনবাবগঞ্জসদর", "name" => "মো: সেরাজুল ইসলাম", "email" => "uiscsundarpurupooo@gmail.com", "phone" => "01740862480"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "চাঁপাইনবাবগঞ্জসদর", "name" => "সুলতানা খাতুন", "email" => "Sultanakhatun43@yahoo.com", "phone" => "01759593366"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "চাঁপাইনবাবগঞ্জসদর", "name" => "মো: মজিবুর রহমান", "email" => "mojibur.u.p.uisc@gmail.com", "phone" => "01827099683"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "চাঁপাইনবাবগঞ্জসদর", "name" => "রুমিয়া খাতুন", "email" => "most.rumia1989@gmail.com", "phone" => "01193005167"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "চাঁপাইনবাবগঞ্জসদর", "name" => "মো: সাদিকুল ইসলাম", "email" => "sadiqulislamup@gmail.com", "phone" => "01716109657"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "চাঁপাইনবাবগঞ্জসদর", "name" => "শিউলি খাতুন", "email" => "shuilykhatun@gmail.com", "phone" => "01763927610"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "চাঁপাইনবাবগঞ্জসদর", "name" => "মো: আমানুল্লাহ", "email" => "ismailm483@gmail.com", "phone" => "01753839757"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "চাঁপাইনবাবগঞ্জসদর", "name" => "রুনা লাইলা", "email" => "runa824@gmail.com", "phone" => "01753227573"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "চাঁপাইনবাবগঞ্জসদর", "name" => "মো: ইসমাইল হোসেন", "email" => "ismailm483@gmail.com", "phone" => "01725622254"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "চাঁপাইনবাবগঞ্জসদর", "name" => "নাহিদা আক্তার বানু", "email" => "nahida.up.uisc@gmail.com", "phone" => "01767418743"],


			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "শিবগঞ্জ", "name" => "মো: নাদিম হাসান", "email" => "nadim.cht@gmail.com", "phone" => "01740855086"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "শিবগঞ্জ", "name" => "আয়েশা বেগম", "email" => "ayesha.uisc@gmail.com", "phone" => "01712164036"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "শিবগঞ্জ", "name" => "মো: রোকন আরী", "email" => "rokonuisc@gmail.com", "phone" => "01722969178"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "শিবগঞ্জ", "name" => "সাবিনা ইয়াসমিন", "email" => "sabinauisc2010@gmail.com", "phone" => "01732057222"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "শিবগঞ্জ", "name" => "মোসা: ডলিয়ারা খাতুন", "email" => "daipukuriauisc@gmail.com", "phone" => "01788143288"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "শিবগঞ্জ", "name" => "মতিন", "email" => "daipukuriauisc@gmail.com", "phone" => "01740876117"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "শিবগঞ্জ", "name" => "মো: সেলিম রেজা", "email" => "salimrezaxision2021@gmail.com", "phone" => "01828140368"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "শিবগঞ্জ", "name" => "আইরিন খাতুন", "email" => "salimrezaxision2021@gmail.com", "phone" => "01722054916"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "শিবগঞ্জ", "name" => "মো: কাউসার আলী", "email" => "kauserali.uisc@gmail.com", "phone" => "01734236465"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "শিবগঞ্জ", "name" => "নাজমা খাতুন", "email" => "chakkirtyup@yahoo.com", "phone" => "01710961648"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "শিবগঞ্জ", "name" => "মো: নাজির হোসেন", "email" => "nazirhossain1539@gmail.com", "phone" => "01722301539"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "শিবগঞ্জ", "name" => "আয়েশা খাতুন", "email" => "mst.ayeasha@gmail.com", "phone" => "01722236690"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "শিবগঞ্জ", "name" => "জাহাংগীর আলম", "email" => "jahangir.uisc1990@gmail.com", "phone" => "01733569143"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "শিবগঞ্জ", "name" => "মুনিরা খাতুন", "email" => "jahangir.uisc1990@gmail.com", "phone" => "01782988530"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "শিবগঞ্জ", "name" => "মো: সহিদুল ইসলাম", "email" => "sohidul124ttc@yahoo.com", "phone" => "01713706510"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "শিবগঞ্জ", "name" => "আম্বিয়া খাতুন", "email" => "ambiajuly@yahoo.com", "phone" => "01740857753"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "শিবগঞ্জ", "name" => "মোহা: আব্দুল মবিন", "email" => "amobinuisc@gmail.com", "phone" => "01738666668"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "শিবগঞ্জ", "name" => "তুখরেজাতুল আজমা", "email" => "daniegam@gmail.com", "phone" => "01738421755"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "শিবগঞ্জ", "name" => "মো: জাহিদ হাসান সুমন", "email" => "jahid.shyampur@gmail.com", "phone" => "01725455980"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "শিবগঞ্জ", "name" => "আমেনা খাতুন", "email" => "jahid.shyampur@gmail.com", "phone" => "01733178572"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "শিবগঞ্জ", "name" => "মো: সেতাউর রহমান", "email" => "up.dhainagar.@yahoo.com", "phone" => "01713924607"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "শিবগঞ্জ", "name" => "নুরনাহার খাতুন", "email" => "up.dhainagar.@yahoo.com", "phone" => "01751656549"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "শিবগঞ্জ", "name" => "মো: কাজী রাসেল", "email" => "emonuisc2010@gmail.com", "phone" => "01723342958"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "শিবগঞ্জ", "name" => "ফাহমিদা ইয়াসমিন", "email" => "fahmidayeasminmukta@gmail.com", "phone" => "01722936141"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "শিবগঞ্জ", "name" => "মো: মমিনুল হক", "email" => "mominul1993@gmail.com", "phone" => "01742018761"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "শিবগঞ্জ", "name" => "বিউটি খাতুন", "email" => "nurul.paka@gmail.com", "phone" => "01742032308"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "শিবগঞ্জ", "name" => "মোসা: সুরভি খাতুন", "email" => "surovikhatun@gmail.com", "phone" => "01767411610"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "শিবগঞ্জ", "name" => "তসিকুল ইসলাম", "email" => "tosiqulislam1994@gmail.com", "phone" => "01740322463"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "শিবগঞ্জ", "name" => "মোসা: মাস্তারা খাতুন", "email" => "mastrakhatun.uisc@gmail.com", "phone" => "01733254011"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "শিবগঞ্জ", "name" => "তাসাউর রহমান", "email" => "sahbajpurupo2@gmail.com", "phone" => "01713705654"],

			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "গোমস্তাপুর", "name" => "মো: অলিউল ইসলাম", "email" => "dalim5588@yahoo.com", "phone" => "01713795588"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "গোমস্তাপুর", "name" => "মরিওম খাতুন", "email" => "moriom6588@gmail.com", "phone" => "01727676588"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "গোমস্তাপুর", "name" => "মো: আলতামাসউদ্দিন", "email" => "gomastapurup@gmail.com", "phone" => "01740883705"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "গোমস্তাপুর", "name" => "শাহীন আক্তার", "email" => "no address", "phone" => "01733936892"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "গোমস্তাপুর", "name" => "মো: আশরাফুল আলম", "email" => "asrafulalam29@yahoo.com", "phone" => "01714549411"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "গোমস্তাপুর", "name" => "রেশমী খাতুন", "email" => "resmiabd@gmail.com", "phone" => "01797646611"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "গোমস্তাপুর", "name" => "মো: দেলোয়ার হোসেন", "email" => "deluwar9591@gmail.com", "phone" => "01736409591"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "গোমস্তাপুর", "name" => "খাতুন", "email" => "rimukhatunuisc@gmail.com", "phone" => "01948736409"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "গোমস্তাপুর", "name" => "মো: আতিকুর রহমান", "email" => "affiaebnath@yahoo.com", "phone" => "01740970524"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "গোমস্তাপুর", "name" => "ইয়াসমিন আখতার", "email" => "affiaebnath@yahoo.com", "phone" => "01748558156"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "গোমস্তাপুর", "name" => "মো: মশিউর রহমান", "email" => "moshiurkr87@gmail.com", "phone" => "01727592784"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "গোমস্তাপুর", "name" => "শিউলী খাতুন", "email" => "shewlykhatunr@gmail.com", "phone" => "01749570731"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "গোমস্তাপুর", "name" => "শ্রী নরেশ চন্দ্র বর্মন", "email" => "noresuisc@gmail.com", "phone" => "01745170481"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "গোমস্তাপুর", "name" => "মতি শংকোরী রানী", "email" => "sonkoriuisc@gmail.com", "phone" => "01776881614"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "গোমস্তাপুর", "name" => "মো: আব্দুল মতিন চঞ্চল", "email" => "upbangabari@gmail.com", "phone" => "01767162521"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "গোমস্তাপুর", "name" => "সাদেকা খাতুন", "email" => "shadekauisc@gmail.com", "phone" => "01794920473"],

			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "নাচোল", "name" => "মো: লিটন আলী", "email" => "suvra2011.bd2021@yahoo.com", "phone" => "01749677689"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "নাচোল", "name" => "শাহিনুর খাতুন", "email" => "shahinuruisc@yahoo.com", "phone" => "01720615608"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "নাচোল", "name" => "শ্রী চন্দন কুমার সিল", "email" => "chandon09@gmail.com", "phone" => "01747202701"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "নাচোল", "name" => "রানী", "email" => "chandon09@gmail.com", "phone" => "01790500250"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "নাচোল", "name" => "মো: আনিকুল ইসলাম", "email" => "md.aniqulislam@gmail.com", "phone" => "01741054525"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "নাচোল", "name" => "নাদিরা বেগম", "email" => "md.aniqulislam@gmail.com", "phone" => "01741054525"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "নাচোল", "name" => "মো: তরিকুল ইসলাম", "email" => "toriqul3344@gmail.com", "phone" => "01739307258"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "নাচোল", "name" => "সাজেদা খাতুন", "email" => "toriqul3344@gmail.com", "phone" => "01795637209"],

			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "ভোলাহাট", "name" => "মো: সুমন আলী", "email" => "sumon1745@gmail.com", "phone" => "01745355715"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "ভোলাহাট", "name" => "সুলতানা খাতুন", "email" => "bholahat-up@yahoo.com", "phone" => "01775875318"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "ভোলাহাট", "name" => "মো: সেলিম রেজা", "email" => "selim.upuisc@gmail.com", "phone" => "01751472329"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "ভোলাহাট", "name" => "সুলতানা খাতুন", "email" => "gohalbari_up@yahoo.com", "phone" => "01763386001"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "ভোলাহাট", "name" => "মো: সেলিম রেজা", "email" => "daldoli_up@yahoo.com", "phone" => "01754785234"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "ভোলাহাট", "name" => "রুকসানা খাতুন", "email" => "daldoli_up@yahoo.com", "phone" => "01751472335"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "ভোলাহাট", "name" => "মো: আতাউল্লাহ", "email" => "jambaria_up@yahoo.com", "phone" => "01719035014"],
			["division" => "রাজশাহী", "dist" => "চাঁপাইনবাবগঞ্জ", "sub_dist" => "ভোলাহাট", "name" => "টুম্পা খাতুন", "email" => "azhamid100@gmail.com", "phone" => "01792426642"]
		];

		$trans	= ["১"=>"1", "২"=>"2", "৩"=>"3", "৪"=>"4", "৫"=>"5", "৬"=>"6", "৭"=>"7", "৮"=>"8", "৯"=>"9", "০"=>"0", "o"=>"0", "O"=>"0"];

		for ($i=0; $i < count($data); $i++) {
			$list['division'] 		= $data[$i]['division'];
			$list['dist'] 			= $data[$i]['dist'];
			$list['sub_dist'] 		= $data[$i]['sub_dist'];
			$list['name'] 			= $data[$i]['name'];
			$list['phone'] 			= strtr($data[$i]['phone'], $trans);
			$list['email'] 			= $data[$i]['email'];
			$list['created_at'] 	= \Carbon\Carbon::now();

			//My_model::insert('entrepreneurs_list', $list);
		}
			return "It's Completed";
   }
}