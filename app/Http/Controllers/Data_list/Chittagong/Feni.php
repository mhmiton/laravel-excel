<?php

namespace App\Http\Controllers\Data_list\Chittagong;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\My_model;

class Feni extends Controller
{
   public function index()
   {
   		
		$data = [
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "ফেনী সদর", "name" => "জনাব মোহাম্মদ আনোয়ার উল্যাহ", "email" => "anawer.ullah85@gmail.com", "phone" => "০১৮১৯-৫০৬৯১২"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "ফেনী সদর", "name" => "জনাব শাহাদাত হোসেন", "email" => "shahadat.feni06@gmail.com", "phone" => "০১৮১৫৮০৭১০৬"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "ফেনী সদর", "name" => "জনাব মোঃ আলী ইমরান", "email" => "someone.tm@gmail.com", "phone" => "০১৮১৩৮৪২০২০"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "ফেনী সদর", "name" => "জনাব আছমা আক্তার", "email" => "asmafeni1987@gmail.com", "phone" => "০১৭৮০৩২২৭৫২"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "ফেনী সদর", "name" => "মোঃ আলী হোসেন", "email" => "ali.hossain80@yahoo.com", "phone" => "০১৮২৮৫৮৬৩২১"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "ফেনী সদর", "name" => "জনাব রহিমা আক্তার", "email" => "dudc2017@gmail.com", "phone" => "০১৮২৯৯৪৭১৩৩"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "ফেনী সদর", "name" => "জনাব মীর হোসেন", "email" => "mirhossainmiru@gmail.com", "phone" => "০১৮১৮৫০২৪০৪"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "ফেনী সদর", "name" => "জনাব নুসরাত জাহান খানম", "email" => "nusratjahan275@yahoo.com", "phone" => "০১৮৫৫৮৯৭৫৭০"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "ফেনী সদর", "name" => "জনাব মহি উদ্দিন", "email" => "mohin74@yahoo.com", "phone" => "০১৮২৫২১৫৯৫৫"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "ফেনী সদর", "name" => "জনাব আছমা আক্তার", "email" => "asmaktershila@yahoo.com", "phone" => "০১৯১৮০৩০৫৮০"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "ফেনী সদর", "name" => "জনাব মো: ওমর ফারুক", "email" => "faruk22449@gmail.com", "phone" => "০১৮১৫০৭৪৪৫০"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "ফেনী সদর", "name" => "জনাব নিপা রানী দাস", "email" => "niparanibd5202@gmail.com", "phone" => "01859-585202"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "ফেনী সদর", "name" => "জনাব ছাবেরা আক্তার", "email" => "sumi.feni1987@gmail.com", "phone" => "01812-969531"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "ফেনী সদর", "name" => "জনাব নিজাম উদ্দিন", "email" => "nizamniloy2010@gmail.com", "phone" => "০১৮২৯-৬০৩৯৪২"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "ফেনী সদর", "name" => "জনাব নাদিম উদ্দিন", "email" => "nadimuddin01@gmail.com", "phone" => "০১৮১৯৭০৭১৮১"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "ফেনী সদর", "name" => "জনাব মাইনুল ইসলাম", "email" => "mdmainul3275@gmail.com", "phone" => "০১৮৩৬৫৪৮৪৭৪"],

			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "ছাগলনাইয়া", "name" => "জনাব জোহরা সুলতানা", "email" => "zohorasultana01@gmail.com", "phone" => "০১৮৩৬৫৪৮৪৭৪"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "ছাগলনাইয়া", "name" => "জনাব মোঃ মাহবুবুল হক", "email" => "rajufenibd@yahoo.com", "phone" => "০১৮২২৮৯৩২৫৪"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "ছাগলনাইয়া", "name" => "জনাব নূর মহল আক্তার", "email" => "shilpe1188@gmail.com", "phone" => "০১৮২০৯২৯৭৯৮"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "ছাগলনাইয়া", "name" => "জনাব শারমীনা আক্তার", "email" => "sarminaakter656@yahoo.com", "phone" => "০১৮৩৯৩৯৭২৩৯"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "ছাগলনাইয়া", "name" => "জনাব সুমন চন্দ্র দাশ", "email" => "sumondasfeni99@gmail.com", "phone" => "০১৮৩৮১৭২৪৭৪"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "ছাগলনাইয়া", "name" => "জনাব মো: শওকত হোসেন", "email" => "showkathossain229@gmail.com", "phone" => "০১৮২১৯৯৪৪৭২"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "ছাগলনাইয়া", "name" => "জনাব শাহীনুর আক্তার", "email" => "ashahinur606@gmail.com", "phone" => "০১৮৩৯৪৩৯৬৮১"],

			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "সোনাগাজী", "name" => "জনাব মুহাম্মদ আমিরুল ইসলাম", "email" => "mdsumon.feni@gmail.com", "phone" => "০১৮৫২৯৩২১২২"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "সোনাগাজী", "name" => "জনাব মোহাম্মদ ইয়াছিন", "email" => "asinhamid2011@gmail.com", "phone" => "০১৬১৫৩১৯৫৮১"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "সোনাগাজী", "name" => "জনাব মো: সালেহ উদ্দিন ফয়সল", "email" => "faisal.beautiful@gmail.com", "phone" => "০১৭১৭৬৪৬৬৩০"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "সোনাগাজী", "name" => "জনাব সাবিনা ইয়াসমিন", "email" => "syasmin6123@gmail.com", "phone" => "১৮৩৬-৫৯৫২০৭"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "সোনাগাজী", "name" => "জনাব সাইফুর রহমান", "email" => "rony.fenibd12@gmail.com", "phone" => "০১৮৩৬৩৯৩২১৮"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "সোনাগাজী", "name" => "জনাব আবু সুফিয়ান", "email" => "n05.char@yahoo.com", "phone" => "০১৮২৬৬৬৮২৮২"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "সোনাগাজী", "name" => "জনাব মোঃ আলা উদ্দিন জুয়েল", "email" => "uddinala18@yahoo.com", "phone" => "০১৭১২৭১৪৭৫১"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "সোনাগাজী", "name" => "জনাব মোঃ জহিরুল ইসলাম", "email" => "amirhossain3036@gmail.com", "phone" => "০১৯৯০৩৬৩৪৩৫"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "সোনাগাজী", "name" => "জনাব ইকবাল হোসেন", "email" => "nababpur@gmail.com", "phone" => "01835-449999"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "সোনাগাজী", "name" => "জনাব দিলরুবা আক্তার", "email" => "nababpur@gmail.com", "phone" => "০১৮৫০০১৪৩৯৯"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "সোনাগাজী", "name" => "জনাব রুবেল চন্দ্র দাস", "email" => "porashkumer@yahoo.com", "phone" => "০১৮২৭৬৪২৬৫০"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "সোনাগাজী", "name" => "জনাব পান্না আক্তার", "email" => "pannaakterorien@gmail.com", "phone" => "০১৮৭৬৯০৭৯০২"],

			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "ফুলগাজী", "name" => "জনাব মো: জাকারিয়া", "email" => "zakaria035@gmail.com", "phone" => "০১৮১৮-৫১২০৪৫"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "ফুলগাজী", "name" => "জনাব নাহিদা সুলতানা", "email" => "fulgaziup.fulgazi@yahoo.com", "phone" => "০১৮৮২১৩৭০১৬"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "ফুলগাজী", "name" => "জনাব আমেনা আক্তার", "email" => "upmunshirhat@gmail.com", "phone" => "০১৮৩৫৯৬৯৫২৪"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "ফুলগাজী", "name" => "জনাব সুমন মজুমদার", "email" => "sumanmajumder99999@gmail.com", "phone" => "০১৭৪৩-৪১২৯১৮"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "ফুলগাজী", "name" => "জনাব বিধান চন্দ্র শীল", "email" => "bidhansheel2015@gmail.com", "phone" => "০১৮১৪২৭৫৮৬৩"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "ফুলগাজী", "name" => "জনাব আছমা আক্তার", "email" => "asma.akter2003@gmail.com", "phone" => "০১৮১৪৩৬৭৮২৭"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "ফুলগাজী", "name" => "জনাব আবুল কাশেম", "email" => "soyed.hassan@gmail.com", "phone" => "০১৮২৯৬৫৫৫৪৫"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "ফুলগাজী", "name" => "জনাব আমজাদ হোসেন", "email" => "anandapurudc@gmail.com", "phone" => "০১৬১৫২৯১১১৬"],

			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "পরশুরাম", "name" => "জনাব আতিকুর রহমান", "email" => "atiqfeni@gmail.com", "phone" => "০১৭২৭৭৫৮৭৪৮"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "পরশুরাম", "name" => "জনাব সাইফুল ইসলাম", "email" => "mirzanagar20uisc@gmail.com", "phone" => "০১৮১৫১২২২২৬"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "পরশুরাম", "name" => "জনাব ফেরদাউস আক্তার", "email" => "ferdousakther5@gmail.com", "phone" => "০১৮১৮২৭৫৯২৭"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "পরশুরাম", "name" => "জনাব মোঃ ইয়াছিন হোসেন মজুমদার", "email" => "yeasinhossen@gmail.com", "phone" => "০১৮১৬৪২৯৫৪৬"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "পরশুরাম", "name" => "জনাব ইসমত জাহান", "email" => "ismot2015@gmail.com", "phone" => "০১৮২২৩৭০৪৬৩"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "পরশুরাম", "name" => "জনাব মোহাম্মদ ইমন", "email" => "adhaninfo14@gmail.com", "phone" => "০১৮১৬৪২৯৮১৪"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "পরশুরাম", "name" => "আফরোজা আক্তার", "email" => "adhaninfo14@gmail.com", "phone" => "০১৮৬৯৮৮৫৩৫০"],

			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "দাগনভূঁঞা", "name" => "জনাব বাবুল চন্দ্র মজুমদার", "email" => "pujabd27@gmail.com", "phone" => "০১৮২৩১৩৩৩১০"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "দাগনভূঁঞা", "name" => "জনাব মোয়াজ্জেম হোসেন", "email" => "uiscyeakubpur@gmail.com", "phone" => "০১৮৪৯৪২২৮৪৬"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "দাগনভূঁঞা", "name" => "জনাব আবু সাঈদ", "email" => "abusayed.rume@gmail.com", "phone" => "০১৮১৪৪৯০৩৩১"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "দাগনভূঁঞা", "name" => "জনাব আব্দুর রব", "email" => "rizono96@gmail.com", "phone" => "০১৮২২০৭৫৯৭২"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "দাগনভূঁঞা", "name" => "জনাব ফাতেমা তুজ জোহরা", "email" => "reaz.uddin85@yahoo.com", "phone" => "০১৮৬১৬০৪৮২২"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "দাগনভূঁঞা", "name" => "জনাব মোবারক হোসেন", "email" => "mranafeni@gmail.com", "phone" => "০১৮৭৮৫৬৬৫৫১"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "দাগনভূঁঞা", "name" => "জনাব ফেরদাউস আরা", "email" => "f_y_labon@yahoo.com", "phone" => "০১৮১৫০৫৫১০৯"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "দাগনভূঁঞা", "name" => "জনাব ফরিদা আক্তার", "email" => "mayordp@yahoo.com", "phone" => "০১৮১৬০৮০৩০৮"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "দাগনভূঁঞা", "name" => "জনাব মুহামম্দ মোয়াজ্জেম হোসাইন", "email" => "mayordp@yahoo.com", "phone" => "০১৮১৯5-৬৬৬৯১"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "দাগনভূঁঞা", "name" => "জনাব নুরজ্জামান", "email" => "uisc789@gmail.com", "phone" => "০১৮১৫০৫৪২০৩"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "দাগনভূঁঞা", "name" => "জনাব আনিকা তাহসিন", "email" => "uisc789@gmail.com", "phone" => "০১৮৫৫৮৯৮০৮১"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "দাগনভূঁঞা", "name" => "জনাব ফরিদ উদ্দিন", "email" => "ontor92@gmail.com", "phone" => "০১৮৭৫৫৭১০১০"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "দাগনভূঁঞা", "name" => "জনাব বিবি কুলসুম", "email" => "uisc73@gmail.com", "phone" => "০১৮৬৯১১৪৮৫১"],
			["division" => "চট্টগ্রাম", "dist" => "ফেনী", "sub_dist" => "দাগনভূঁঞা", "name" => "জনাব প্রশান্ত কুমার মজুমদার", "email" => "pcpupbd@yahoo.com", "phone" => "০১৮১৬৫৫৮৫১২"]

		];

		$trans	= ["১"=>"1", "২"=>"2", "৩"=>"3", "৪"=>"4", "৫"=>"5", "৬"=>"6", "৭"=>"7", "৮"=>"8", "৯"=>"9", "০"=>"0", "o"=>"0", "O"=>"0"];

		for ($i=0; $i < count($data); $i++) {
			$list['division'] 		= $data[$i]['division'];
			$list['dist'] 			= $data[$i]['dist'];
			$list['sub_dist'] 		= $data[$i]['sub_dist'];
			$list['name'] 			= $data[$i]['name'];
			$list['phone'] 			= strtr($data[$i]['phone'], $trans);
			$list['email'] 			= $data[$i]['email'];
			$list['created_at'] 	= \Carbon\Carbon::now();

			//My_model::insert('entrepreneurs_list', $list);
		}
			return "It's Completed";
   }
}