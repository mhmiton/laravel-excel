<?php

namespace App\Http\Controllers\Data_list\Khulna;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\My_model;

class Satkhira extends Controller
{
   public function index()
   {
   		
		$data = [
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "সাতক্ষীরাসদর", "name" => "মো: মোগফুর রহমান", "phone" => "০১৭১৭৭৬৯৫৫৯", "email" => "mdmogfurrahman@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "সাতক্ষীরাসদর", "name" => "মোছা: মিনু সূলতানা", "phone" => "০১৭২৩৯২০৭৭৯", "email" => "minu.uisc@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "সাতক্ষীরাসদর", "name" => "মো: সাইফুজ্জামান", "phone" => "০১৭৪৮৯৯৩৮২৫", "email" => "saifuzzaman.uisc@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "সাতক্ষীরাসদর", "name" => "মেহেরুন্নেছা রিনা", "phone" => "০১৭৪৬০৪১৫৪১", "email" => "rina.uisc@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "সাতক্ষীরাসদর", "name" => "রেজাওয়ানুল্লা", "phone" => "০১৭৪৮৯১২৪৭৯", "email" => "rezwan_nu@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "সাতক্ষীরাসদর", "name" => "হাজিরা খাতুন", "phone" => "০১৭৪৮৭৮০৭৫৭", "email" => "joynab.uisc@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "সাতক্ষীরাসদর", "name" => "মো: মনিরুল ইসলাম", "phone" => "০১৭৪০৯২০৭৬২", "email" => "moni_ghona@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "সাতক্ষীরাসদর", "name" => "সেলিনা জেসমিন", "phone" => "০১৭৫২০৪৫২88", "email" => "jasmin_ghona@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "সাতক্ষীরাসদর", "name" => "মোঃ কবিরুল ইসলাম", "phone" => "০১৭১৫৫৪৮৯৬৫", "email" => "mdkobirul87@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "সাতক্ষীরাসদর", "name" => "মোছা: খাদিজা সুলতানা", "phone" => "০১৭৪৮৪৮১১১৫", "email" => "khadijasultana29@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "সাতক্ষীরাসদর", "name" => "মো: শহিদুল ইসলাম", "phone" => "০১৯২২৮৭০৪৩৮", "email" => "shislam2@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "সাতক্ষীরাসদর", "name" => "ইয়াসমিন সুলতানা", "phone" => "০১৯২২৮৭০৪৩৮", "email" => "shislam2@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "সাতক্ষীরাসদর", "name" => "মো: আলমগীর হোসেন", "phone" => "০১৭১৯৪৮১৬৪৪", "email" => "s80alam@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "সাতক্ষীরাসদর", "name" => "শিরিন সুলতানা", "phone" => "০১৯৩৪৮০৮৮৪৯", "email" => "sarmin1971@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "সাতক্ষীরাসদর", "name" => "মো: আতাউর রহমান", "phone" => "০১৯১৭৬৭৪২১১", "email" => "ataur7k@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "সাতক্ষীরাসদর", "name" => "দিপালী রানী রায়", "phone" => "০১৭৫০৯১৭৬২৬", "email" => "dipa7k@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "সাতক্ষীরাসদর", "name" => "মো: আলমগীর হোসেন", "phone" => "০১৭২২৩৫৯৯৩৪", "email" => "ahalamgir5@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "সাতক্ষীরাসদর", "name" => "মো: কামরুল হাসান", "phone" => "০১৭৬৬৭৫৭৪৭৩", "email" => "kamrulhassan402@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "সাতক্ষীরাসদর", "name" => "স্বপ্না ইয়াসমিন", "phone" => "০১৭২৭৯০৭৮৭৭", "email" => "kamrulhassan402@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "সাতক্ষীরাসদর", "name" => "আব্দুল মোমিন", "phone" => "০১৭৫৩১০৩০৪৪", "email" => "momin.satkhira@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "সাতক্ষীরাসদর", "name" => "সানজিয়ারা খাতুন", "phone" => "০১৯২১৭৭৭১৯৯", "email" => "tania.hazipur@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "সাতক্ষীরাসদর", "name" => "আলমগীর হোসেন", "phone" => "০১৭২৯৮৮০৪৪০", "email" => "alamgir.uisc7@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "সাতক্ষীরাসদর", "name" => "রেহেনা খাতুন", "phone" => "০১৭২৬৪২০৩০২", "email" => "rehana.uisc80@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "সাতক্ষীরাসদর", "name" => "মোঃইকবাল হোসেন", "phone" => "০১৭৪৪৭৬৪১৪৩", "email" => "iqbalsat2011@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "সাতক্ষীরাসদর", "name" => "শাহানাজ পারভীন", "phone" => "০১৭৩৭২৬৫৩৮৬", "email" => "shahanaz62@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "সাতক্ষীরাসদর", "name" => "আলমগীর আজাদ", "phone" => "০১৭১৪৩৫৪১৪৮", "email" => "alamsatkhira1@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "সাতক্ষীরাসদর", "name" => "নিলুফার ইয়াসমিন", "phone" => "০১৭১৮১০১৫২৩", "email" => "alamgirazad7@gmail.com"],

			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কলারোয়া", "name" => "মিঠুন সাহা", "phone" => "০১৭১৪৮৪৬০২৩", "email" => "mithunsaha7@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কলারোয়া", "name" => "শিউলিখাতুন", "phone" => "০১৭৯৮২২৯৮৬২", "email" => "sheuli.udc@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কলারোয়া", "name" => "মো: ইমাদুল হক", "phone" => "০১৭৪২১৭১২১০", "email" => "imadul_haque@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কলারোয়া", "name" => "শারমিনসুলতানা", "phone" => "০১৭০৬৪৪২১৯৬", "email" => "imadul.uisc@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কলারোয়া", "name" => "মোঃ আঃ আহাদ", "phone" => "০১৯২৪৩২৭৩৬২", "email" => "ahad.uisc@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কলারোয়া", "name" => "মিনারা খাতুন", "phone" => "০১৭৩৭৭৩৩৯৮০", "email" => "miniara@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কলারোয়া", "name" => "আমিরুল ইসলাম", "phone" => "০১৯২৭৮৯৫০৫৭", "email" => "iamirul869@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কলারোয়া", "name" => "মোছাঃ নাজমা পারভীন", "phone" => "০1735036929", "email" => "ramoni.sultana@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কলারোয়া", "name" => "আসাদুল ইসলাম", "phone" => "০১৭৮৫০৭৯১৭১", "email" => "asadul.uisc@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কলারোয়া", "name" => "নাসরিন আক্তার", "phone" => "০১৭৫২৩৮১০৩৫", "email" => "nasrin.uisc89@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কলারোয়া", "name" => "মো: জাহিদুর রহমান", "phone" => "০১৭২৩৮৬৫৪২০", "email" => "zahidur.uisc@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কলারোয়া", "name" => "মোছাঃ সুলতানা পারভীন", "phone" => "০1751412970", "email" => "sagaruisc1@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কলারোয়া", "name" => "মোঃ ইকবাল হোসেন", "phone" => "০১৭২৮৯০৩৯০২", "email" => "iqbal.kakaroa@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কলারোয়া", "name" => "মোছা: সেলিনা খাতুন", "phone" => "০১৭৭৯১৫২১৩৩", "email" => "ozufa.uisc@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কলারোয়া", "name" => "জিএম আব্দুল আজিজ", "phone" => "০১৯২১৮১৬৯৮৯", "email" => "gmaziz.uisc@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কলারোয়া", "name" => "মুক্তা খাতুন", "phone" => "০১৭৮৬১০৪০২০", "email" => "nurunnahar.uisc@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কলারোয়া", "name" => "আক্তারুল ইসলাম", "phone" => "০১৭৩০৬৬৯১১৩", "email" => "aktarul11@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কলারোয়া", "name" => "ময়না খাতুন", "phone" => "০১৭৩১৬৯৬৫৪২", "email" => "moynabd7@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কলারোয়া", "name" => "মো: সেলিম হোসেন", "phone" => "০১৭৫২১১১৮৫২", "email" => "salim10ku@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কলারোয়া", "name" => "মোছা: নিলুফার ইয়াছমিন", "phone" => "০১৯২২৩৫৬৫৭২", "email" => "salim79ku@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কলারোয়া", "name" => "মোঃ জসীমউদ্দীন", "phone" => "০১৯১৮২৯৫৬৫৬", "email" => "jashimplanet@gmail.con"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কলারোয়া", "name" => "মোছা: রুমা", "phone" => "০১৭২৬০৬৭৪৩২", "email" => "juiplanet347@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কলারোয়া", "name" => "মো: আ: খালেক", "phone" => "০১৭৩৪৭৮৫৪১৫", "email" => "akhaleq@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কলারোয়া", "name" => "মো: রাহিলা আক্তার", "phone" => "০১৭৩৪৯৫২৬৩০", "email" => "rahilaakter@yahoo.com"],

			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "তালা", "name" => "মো: সহিদুল ইসলাম", "phone" => "০১৭১৪৯০৫৭৪১", "email" => "shahidul12@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "তালা", "name" => "মোছা: তৃষা খাতুন", "phone" => "০1735874647", "email" => "dhandia.uisc@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "তালা", "name" => "মো: লিমন খান", "phone" => "০১৭১৭২৪৯৯৩৩", "email" => "lemonkhan55@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "তালা", "name" => "শরিফা সিদ্দিকা", "phone" => "০১৭১৬০২০৬৯৬", "email" => "siddika000@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "তালা", "name" => "জাহাঙ্গীর আলম", "phone" => "০১৭২৫৩৫১৬৯৭", "email" => "jalam.uisc@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "তালা", "name" => "রাকিবা খাতুন", "phone" => "০১৯২০৩২৩৩৫৯", "email" => "Jalam.uisc@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "তালা", "name" => "শহিদুল ইসলাম", "phone" => "০১৭২৬২৫৮৬৯৮", "email" => "sahidul.uisc@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "তালা", "name" => "নিলুফার ইয়াসমিন", "phone" => "০১৭৪৭৭৭৭৯৮৫", "email" => "n.uisc@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "তালা", "name" => "অপূর্ব দাস", "phone" => "০১৭৩৯০০৫৪০৩", "email" => "apurba.uisc@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "তালা", "name" => "আফরোজা খাতুন", "phone" => "০১৭৫১৫৫৪৩৫৬", "email" => "afroga.uisc@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "তালা", "name" => "মোঃ আসাদুজ্জামান", "phone" => "০১৭২১৯৪৭৭৯৫", "email" => "mdasaduzzaman60@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "তালা", "name" => "রোকেয়া খাতুন", "phone" => "০1724705701", "email" => "rokaya.uisc@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "তালা", "name" => "আহসান ফেরদৌস", "phone" => "০১৭২৩৮৬৫২১৬", "email" => "ahosan2012@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "তালা", "name" => "আশরাফ হোসেন", "phone" => "০১৭২৪৪৬০০৪৭", "email" => "asraf.uisc@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "তালা", "name" => "ময়না খাতুন", "phone" => "01757841975", "email" => "rafiza.uisc@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "তালা", "name" => "শেখ আব্দুল্ল্যাহ আল-আমিন", "phone" => "০১৭১২৪৪৮৮২১", "email" => "uiscronybd@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "তালা", "name" => "জেসমিন নাহার", "phone" => "০১৭১৪২১০৪৩৭", "email" => "jesmin.uisc@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "তালা", "name" => "মো: বিল্লাল হোসেন", "phone" => "০১৭১৯০১৯২৭০", "email" => "ballal.uisc@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "তালা", "name" => "মো: মঈনুর হোসেন", "phone" => "০১৭২৮৫৩৪১১৯", "email" => "moinur.uisc@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "তালা", "name" => "স্বপ্না বিশ্বাস", "phone" => "০১৭৪০৬১৬৩৯৯", "email" => "swapna.uisc@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "তালা", "name" => "মো: মনিরুল ইসলাম", "phone" => "০1721689689", "email" => "gm.monirul@yahoo.com"],

			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "আশাশুনি", "name" => "নাজমা খাতুন", "phone" => "০১৯২৫৫৫৮৪৯১", "email" => "nazma1.uisc@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "আশাশুনি", "name" => "মো: মাসুদ করিম", "phone" => "০১৭৪৭১৫৩৭৭৫", "email" => "masudkarim97@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "আশাশুনি", "name" => "সর্বানী সানা", "phone" => "০১৭৫২১১২০৯১", "email" => "sarbanisana.uisc@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "আশাশুনি", "name" => "মোঃ বেলাল হোসেন", "phone" => "০১৭২৭৯০৮৩৩৬", "email" => "bilalsatkhirabd@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "আশাশুনি", "name" => "রহিমা খাতুন", "phone" => "০১৭৩৫৬৮৭১০৪", "email" => "rahimakhatun257@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "আশাশুনি", "name" => "রবিউল ইসলাম", "phone" => "০১৭৪৫৭০৩৮৬৫", "email" => "rabiul.islamkullaup@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "আশাশুনি", "name" => "Avwgbyi Bmjvg", "phone" => "01756887652", "email" => "aminur.uisc@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "আশাশুনি", "name" => "সাজেদা পারভীন", "phone" => "০১৭২৪৪৩৪২১৫", "email" => "sajedakhatun.uisc@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "আশাশুনি", "name" => "দেব্রত কুমার মন্ডল", "phone" => "০১৭৩১৯৬১2৩১", "email" => "devbrota@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "আশাশুনি", "name" => "তৈয়েবুর রহমান", "phone" => "০১৯১৬১১০৩১২", "email" => "shadin.uisc@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "আশাশুনি", "name" => "শরিফা খাতুন", "phone" => "০১৭৩৫০২১৪৩৬", "email" => "shadin.uisc@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "আশাশুনি", "name" => "হিমাংশু মন্ডল", "phone" => "০১৭২১৭৬0৭৬৯", "email" => "himangsu707@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "আশাশুনি", "name" => "শিরিনা আক্তার", "phone" => "01747823044", "email" => "shrabanti707@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "আশাশুনি", "name" => "ইকবাল হোসেন", "phone" => "০১৯১৯১৩১৬৫০", "email" => "ikbal1971@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "আশাশুনি", "name" => "নাছিমা খাতুন", "phone" => "০১৯১২২৯৩৫৭১", "email" => "nasimakhatun.0@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "আশাশুনি", "name" => "শাহীনুর রহমান", "phone" => "০১৯১৭৮৬২১৫২", "email" => "shahin.uisc88@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "আশাশুনি", "name" => "শামিমা আলম", "phone" => "01956267156", "email" => "arefa.uisc@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "আশাশুনি", "name" => "মো: সামছুল আলম", "phone" => "০১৯২৭৪৫৮৯৪৪", "email" => "bappihasan@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "আশাশুনি", "name" => "দেব্রদাস কুমার সানা", "phone" => "০১৭২৭৮১৩৮৯১", "email" => "debdassana@gmail.com"],

			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "দেবহাটা", "name" => "অলকা সানা", "phone" => "01788294830", "email" => "aloka.sana@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "দেবহাটা", "name" => "মো: মাহমুদুল হোসেন", "phone" => "01746858386", "email" => "mahmudworld.bd@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "দেবহাটা", "name" => "আফিফা সুলতানা", "phone" => "০১৭২৮৪৫৬০৯৩", "email" => "afifa.uisc1986@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "দেবহাটা", "name" => "মো: মাহবুব আলম", "phone" => "01917950202", "email" => "babu.uisc@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "দেবহাটা", "name" => "শেখ জাহিদ হোসেন", "phone" => "০১৭১৮৪৪৬১৪১", "email" => "zahid.alam84@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "দেবহাটা", "name" => "পুরবী পারভীন", "phone" => "01729311133", "email" => "shampa.rani@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "দেবহাটা", "name" => "মো: জাকির হোসেন", "phone" => "01729656097", "email" => "khalid_stk@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "দেবহাটা", "name" => "লুৎফুন্নাহার", "phone" => "01723815645", "email" => "durga.uiscsatkhira@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "দেবহাটা", "name" => "হারুন-অর-রশিদ", "phone" => "০১৯২৫২১৯৬১১", "email" => "kmharunstk@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "দেবহাটা", "name" => "মোছা: তামান্না পারভীন", "phone" => "০১৭২০৯৯৮১৪০", "email" => "debhataup@gmail.com"],

			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কালিগঞ্জ", "name" => "মো: সাজ্জাদ হোসেন", "phone" => "০১৭১৯৮১৪০৭৮", "email" => "sazzadatoz@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কালিগঞ্জ", "name" => "সাহানা সুলতানা", "phone" => "01740902364", "email" => "nasimaparvin25@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কালিগঞ্জ", "name" => "মো: আব্দুর রহমানমোড়ল", "phone" => "০1622416822", "email" => "Ontim.babil@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কালিগঞ্জ", "name" => "পলাশ মুখার্জী", "phone" => "০১৭৪৬২১৫৬৪০", "email" => "palashkumuarmukharje@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কালিগঞ্জ", "name" => "কানিজ ফারহানা লাবনী", "phone" => "০১৭১৪৬৭২৮৮৭", "email" => "laboni887@gmail.com"] ,
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কালিগঞ্জ", "name" => "ফিরোজা খাতুন", "phone" => "01৭৪৯৩৫৪০২৮", "email" => "sannua.moom7@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কালিগঞ্জ", "name" => "বসুদেব সরকার", "phone" => "০১৭২৭০৮০৩৭৭", "email" => "bosudebsarkar@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কালিগঞ্জ", "name" => "পূজা সরকার", "phone" => "01৭৫১৯৭৪০৩৫", "email" => "tasminara2012@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কালিগঞ্জ", "name" => "শিমুল কুমার দাশ", "phone" => "০১৭১২৯৪৯৩৪০", "email" => "Simul.naltaudc@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কালিগঞ্জ", "name" => "মাকসুরা খাতুন", "phone" => "01৯২২৮৬৭৭৯৩", "email" => "nasimauisc06@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কালিগঞ্জ", "name" => "মো: জয়নাল আবেদিন", "phone" => "০১৯৩১৬৪২২৯৫", "email" => "joynalabidinuisc@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কালিগঞ্জ", "name" => "সারমিন সুলতানা", "phone" => "০১৯৪৯২২১৪১৫", "email" => "shamsunnaharpoly.uisc@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কালিগঞ্জ", "name" => "মো: শাহিন আলম", "phone" => "০১৯৪৩৪৯৭৯১৭", "email" => "shahin.uisc@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কালিগঞ্জ", "name" => "মোঃ জাহাঙ্গীর আলম", "phone" => "০১৭৫১৯৭৪০৩৭", "email" => "jahangir.charda@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কালিগঞ্জ", "name" => "জি এম নুরুল ইসলাম", "phone" => "০১৭৩১০৮১৭৯৩", "email" => "abdulla.minigskty@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কালিগঞ্জ", "name" => "তাছনিম আরা সুলতানা", "phone" => "০১৯৪০৩৫৭২৪১", "email" => "tasnim1971@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কালিগঞ্জ", "name" => "এম মুন্জুর আহমেদ", "phone" => "০১৭১৫২১১৬৫৮", "email" => "monjuakash@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কালিগঞ্জ", "name" => "শেখ আল-আমিন", "phone" => "০১৯১৫৭৬৭৭৮০", "email" => "alamin.hossain42@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "কালিগঞ্জ", "name" => "মোছা: রেহানা পারভীন", "phone" => "০১৯১২৮৬৭৬০৯", "email" => "rinapervin33@yahoo.com"],

			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "শ্যামনগর", "name" => "মো: বাবুল হোসেন", "phone" => "০১৭২৭০৬০২০৭", "email" => "babul.uisc199@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "শ্যামনগর", "name" => "মিসমুসলিমা", "phone" => "01966994251", "email" => "muslimatoufiq@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "শ্যামনগর", "name" => "মো: কামরুজ্জামান", "phone" => "০১৭১৬৪০১৩১৩", "email" => "kamruzzaman.uisc@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "শ্যামনগর", "name" => "ময়না পারভীন", "phone" => "01743108802", "email" => "rehana.uisc@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "শ্যামনগর", "name" => "মো: আব্দুর রশীদ", "phone" => "১৭১০১২০৭১৯", "email" => "marashid.uisc@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "শ্যামনগর", "name" => "মোছাঃ মর্জিনা খাতুন", "phone" => "০১৯১৫০৪৮৪০৯", "email" => "Kulsum.uisc@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "শ্যামনগর", "name" => "মো: রাশেদুল ইসলাম", "phone" => "0১৭১০৯৬২২৫৯", "email" => "rashedul.uisc@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "শ্যামনগর", "name" => "সাবিনাপারভীন", "phone" => "01919962259", "email" => "sabinap773@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "শ্যামনগর", "name" => "মো: ফারুক হোসেন", "phone" => "0১৭৪৮৪৮১২৮২", "email" => "faruque.uisc@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "শ্যামনগর", "name" => "সাহারাবানু", "phone" => "01993532696", "email" => "sahara@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "শ্যামনগর", "name" => "মো: ফিরোজ খান", "phone" => "0১৭৩৪৩৬৭৪৪৭", "email" => "khan.uisc@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "শ্যামনগর", "name" => "মোছাঃ জেসমিন নাহার", "phone" => "01912296206", "email" => "jasmin_uisc@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "শ্যামনগর", "name" => "প্রবীর কর্মকার", "phone" => "0১৭১৭০১১২৪৪", "email" => "probir.uisc@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "শ্যামনগর", "name" => "মো: সোহেল", "phone" => "0১৯১১৪৬২৫৭৮", "email" => "e.uisc@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "শ্যামনগর", "name" => "মো: এনামুল হক", "phone" => "০১৯১৯৭৪৫৭৬১", "email" => "anamul.uisc@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "শ্যামনগর", "name" => "মো: রবিউল ইসলাম", "phone" => "০১৯১২২৯২০৫৬", "email" => "robiulislam772@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "শ্যামনগর", "name" => "জেসমিন", "phone" => "01952470302", "email" => "marufaakter.uisc@yahoo.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "শ্যামনগর", "name" => "আশিকউল্লাহ", "phone" => "০১৯১৬১০৫৫৫৩", "email" => "ashikullah1986@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "শ্যামনগর", "name" => "মোছাঃ সালমা খাতুন", "phone" => "০১৯১৮-৩৭৪৪১২", "email" => "uisc.salma@gmail.com"],
			["division" => "খুলনা", "dist" => "সাতক্ষীরা", "sub_dist" => "শ্যামনগর", "name" => "মো: কবির হোসেন", "phone" => "0১৯৬৪১৬৬৫৮৫", "email" => "kabir.uisc@gmail.com"]
		];

		$trans	= ["১"=>"1", "২"=>"2", "৩"=>"3", "৪"=>"4", "৫"=>"5", "৬"=>"6", "৭"=>"7", "৮"=>"8", "৯"=>"9", "০"=>"0", "o"=>"0", "O"=>"0"];

		for ($i=0; $i < count($data); $i++) {
			$list['division'] 		= $data[$i]['division'];
			$list['dist'] 			= $data[$i]['dist'];
			$list['sub_dist'] 		= $data[$i]['sub_dist'];
			$list['name'] 			= $data[$i]['name'];
			$list['phone'] 			= strtr($data[$i]['phone'], $trans);
			$list['email'] 			= $data[$i]['email'];
			$list['created_at'] 	= \Carbon\Carbon::now();

			//My_model::insert('entrepreneurs_list', $list);
		}
			return "It's Completed";
   }
}