<?php

namespace App\Http\Controllers\Data_list\Barisal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\My_model;

class Pirojpur extends Controller
{
   public function index()
   {
   		
		$data = [
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "নেছারাবাদ", "name" => "সালমান ইসলাম", "phone" => "০১৭৩৩১০১৭০৯", "email" => "ahamirhossen@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "নেছারাবাদ", "name" => "সম্পা আক্তার", "phone" => "০১৭৮২২৩৭৮৯১", "email" => "akter_shampa@yahoo.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "নেছারাবাদ", "name" => "খাদিজা", "phone" => "০১৭১২৯৪৭৫২৭", "email" => "khadizatahera1986@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "নেছারাবাদ", "name" => "মোঃ কবির হোসেন", "phone" => "০১৭৪৮৭৮৭১৫৪", "email" => "hossainkabir747@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "নেছারাবাদ", "name" => "মোসা: আসমা জাহান", "phone" => "০১৭৩৭৩৩১৫৫৭", "email" => "kurianaup@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "নেছারাবাদ", "name" => "বিকাশ মিস্ত্রী", "phone" => "০১৯২৩৬৭২৮০৫", "email" => "bikashmistry886@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "নেছারাবাদ", "name" => "ইছাহাক হোসেন", "phone" => "০১৭৩৪৩০৪৯৪৬", "email" => "ishakhossain6@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "নেছারাবাদ", "name" => "মোসাঃ তানিয়া", "phone" => "০১৯৩৩৭৭৯৮৮৪", "email" => "uddinkamal72@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "নেছারাবাদ", "name" => "বাবুল কৃষ্ণ মন্ডল", "phone" => "০১৭১১৯৭৬০৫৬", "email" => "babulmandal1982@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "নেছারাবাদ", "name" => "মোসাঃ সুমা আক্তার", "phone" => "০১৭৩৪২৬৬২১২", "email" => "mstsum063@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "নেছারাবাদ", "name" => "সুভার্থি হালদার", "phone" => "০১৭২৮৩৬৭২৬১", "email" => "suverthihalder143@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "নেছারাবাদ", "name" => "বিথী সুতার", "phone" => "০১৭৩৮৩২৪৯৭৭", "email" => "bithisutar@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "নেছারাবাদ", "name" => "বুলু রানী বৈদ্য", "phone" => "০১৭৭৪৩০৮৭১৩", "email" => "mintubepari980@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "নেছারাবাদ", "name" => "মিন্টু বেপারী", "phone" => "০১৭২৭০৭৭৫৬১", "email" => "mintubepari980@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "নেছারাবাদ", "name" => "মোঃ মহববত আলী", "phone" => "০১৭৬৩৯০৩৮৪৫", "email" => "alimahabbat25@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "নেছারাবাদ", "name" => "রহিমা খানম", "phone" => "০১৭২৪৯১৭২০৫", "email" => "abdulhaque802@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "নেছারাবাদ", "name" => "সিদ্ধার্থ মন্ডল", "phone" => "০১৭১৯৫৯১০০৭", "email" => "sidhertho@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "নেছারাবাদ", "name" => "সঞ্জিতা সমদ্দার", "phone" => "০১৭৪৬৩০৭২৩৫", "email" => "sanjita2863@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "নেছারাবাদ", "name" => "মোঃ মান্না মেহেদী", "phone" => "০১৭১২৪০৮৭৭৮", "email" => "mannamehedi85@yahoo.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "নেছারাবাদ", "name" => "শারমিন আক্তার", "phone" => "০১৭২৮৭৮০০৫৪", "email" => "sharminshila647@gmail.com"],

			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "কাউখালী", "name" => "মোঃ রেজাউল করিম", "phone" => "০১৭৯৯৩৬১৮৮৪", "email" => "mdrezaulkarim1884@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "কাউখালী", "name" => "শেখ আসাদুজ্জামান", "phone" => "০১৭১২৪৩৬৭৪২", "email" => "sheikhshajib.ss@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "কাউখালী", "name" => "সাদিয়া আক্তার", "phone" => "০১৭৭২৭৯১২০৯", "email" => "shaikatguho@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "কাউখালী", "name" => "সারমিন আক্তার", "phone" => "০১৭১১১২৮৭৮৯", "email" => "sharminakther8510@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "কাউখালী", "name" => "মোঃ মাহাতাব উদ্দিন খান", "phone" => "০১৭১০৬১৪১৩৬", "email" => "zinia8510@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "কাউখালী", "name" => "জিনিয়া আক্তার", "phone" => "০১৭৩৮০০৪২৭৬", "email" => "zinia8510@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "কাউখালী", "name" => "শারমিন সুলতানা", "phone" => "০১৮৩৯২৭৭৩২৬", "email" => "sarminsultana277326@gmail.com"],

			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "ইন্দুরকানী", "name" => "মোঃ আবুল বাশার", "phone" => "০১৭১৬৪৪৬৭০৮", "email" => "p.digital099@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "ইন্দুরকানী", "name" => "ফাহিমা", "phone" => "০১৭৭৮৬০২৪০৪", "email" => "fahemaakter867@yahoo.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "ইন্দুরকানী", "name" => "মোঃ শাহ আলম", "phone" => "০১৭১৮৮১৪১৯৪", "email" => "toha.zia@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "ইন্দুরকানী", "name" => "তানিয়া সুলতানা", "phone" => "০১৭০৪৭১০৯৩০", "email" => "toha.zia@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "ইন্দুরকানী", "name" => "মোঃ কামাল উদ্দিন", "phone" => "০১৭৩০১১২২১৯", "email" => "uddinkamal72@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "ইন্দুরকানী", "name" => "বেগম তাজরিয়ান", "phone" => "০১৭৪৮৭২৪৬২৭", "email" => "uddinkamal72@gmail.com"],

			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "ভান্ডারিয়া", "name" => "গাজী মোঃ সহীদুল ইসলাম বাবুল", "phone" => "০১৭১১২৬৫৩৪১", "email" => "gazibadul78@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "ভান্ডারিয়া", "name" => "মোঃ আঃ রাজ্জাক", "phone" => "০১৮১১৮৫৩০৭৯", "email" => "arazzaq1982@yahoo.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "ভান্ডারিয়া", "name" => "মোঃ শহীদ হোসেন", "phone" => "০১৭১৩৯৫০৩৪৫", "email" => "shahidhissen55@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "ভান্ডারিয়া", "name" => "সাথী আক্তার", "phone" => "০১৭২৬১৮৪৩৬৮", "email" => "sathiakter42@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "ভান্ডারিয়া", "name" => "মোঃ শামীম", "phone" => "০১৭২৪৭২৯২২৭", "email" => "redwanulislamslar@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "ভান্ডারিয়া", "name" => "ইসরাত জাহান মনি", "phone" => "০১৭৩৬৯১৪৭০৮", "email" => "moneakter693@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "ভান্ডারিয়া", "name" => "মোঃ সোহাগ মৃধা", "phone" => "০১৭৪৬৩০৮১৬৪", "email" => "shohagmridha23@ gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "ভান্ডারিয়া", "name" => "লিজা", "phone" => "০১৭২৫৮৩৪৮২৬", "email" => "lizarahoman3@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "ভান্ডারিয়া", "name" => "মোঃ এনায়েত কবির", "phone" => "০১৭২১৮১০৬৮৩", "email" => "enayetkabir101@yahoo.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "ভান্ডারিয়া", "name" => "মোঃ মাহবুব খান", "phone" => "০১৭১৫২৬৭৯৮৫", "email" => "mahbubuisc98@gmail.com"],

			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "মঠবাড়িয়া", "name" => "মরিয়ম আক্তার", "phone" => "০১৭৫৬১৭৪৬৮৬", "email" => "msmariom@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "মঠবাড়িয়া", "name" => "মোঃ নজরুল ইসলাম", "phone" => "০১৭১৬১৯১০১৮", "email" => "islamnazrul788@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "মঠবাড়িয়া", "name" => "সাদিকুর রহমান", "phone" => "০১৭৫৯৭৬০৬৬৫", "email" => "sadiqur2017@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "মঠবাড়িয়া", "name" => "ফারহানা ইসলাম", "phone" => "০১৭৬৬৪৭৩১৫৫", "email" => "sadiqur2017@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "মঠবাড়িয়া", "name" => "হাদিয়া আক্তার", "phone" => "০১৭৩৭১৭৩৮৬৪", "email" => "hadiaakter98@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "মঠবাড়িয়া", "name" => "মোঃ পারভেজ তালুকদার", "phone" => "০১৭৪০৫৮৩২৯২", "email" => "talukderystyle2@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "মঠবাড়িয়া", "name" => "মোঃ ফেরদৌস", "phone" => "০১৭১৫১৬৩৭৪২", "email" => "daudkhaliup@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "মঠবাড়িয়া", "name" => "লিমা", "phone" => "০১৭৭৭৪৬২৪২৩", "email" => "farjanastudio@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "মঠবাড়িয়া", "name" => "মোঃ আউয়াল মিয়া", "phone" => "০১৭৩৪৩৮৬০৬৯", "email" => "aual.sharif88@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "মঠবাড়িয়া", "name" => "তোহা বিনতে রহমান তামান্না", "phone" => "০১৭৯১৪১২৯৯২", "email" => "mahmud.hasan1982@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "মঠবাড়িয়া", "name" => "ফাতিমা আক্তার", "phone" => "০১৭৯৯২৪৫৫২৩", "email" => "fatima.akter36@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "মঠবাড়িয়া", "name" => "সুনিল কুমার দাস", "phone" => "০১৭২৯১১২৮৮৪", "email" => "sunildasbd@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "মঠবাড়িয়া", "name" => "পারভীন খানম", "phone" => "০১৭৭৪৯০৫৮৬৬", "email" => "betmorerajpara@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "মঠবাড়িয়া", "name" => "হ্যাপী আখতার", "phone" => "০১৭০৩০৪০২০০", "email" => "misshappyakter1994@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "মঠবাড়িয়া", "name" => "মিজানুর রহমান", "phone" => "০১৭৩৫৪৬৫১৬২", "email" => "mizanurmizan62@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "মঠবাড়িয়া", "name" => "মোঃ জয়নুল আবেদীন", "phone" => "০১৮৩১৩৪১৭৮২", "email" => "sentumia11@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "মঠবাড়িয়া", "name" => "মোসাঃ সারমীন সুলতানা", "phone" => "০১৭১৩৯৫২২৮১", "email" => "sirminsulatana017@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "মঠবাড়িয়া", "name" => "মাধবী রানী", "phone" => "০১৭৯০৯০৫১৬২", "email" => "madhabi9062@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "মঠবাড়িয়া", "name" => "মো: রনি", "phone" => "০১৭৪২৪০২৬৮৩", "email" => "madhabi9062@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "মঠবাড়িয়া", "name" => "মোঃ জুলকাফল কবির", "phone" => "০১৭১৭১২৬০০১", "email" => "julukabir@gmail.com"],

			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "পিরোজপুর সদর", "name" => "চয়ন মজুমদার", "phone" => "০১৭২৫৫৬১৯৩৪", "email" => "chayonmojumder140@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "পিরোজপুর সদর", "name" => "নমিতা সিকদার", "phone" => "০১৭৩২৪১০৩৫৩", "email" => "namitasikder353@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "পিরোজপুর সদর", "name" => "মিহির মন্ডল", "phone" => "০১৯১৭০৬১২৪৪", "email" => "mihirmandal2050@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "পিরোজপুর সদর", "name" => "সাদিয়া ইসলাম", "phone" => "০১৭৫৯৩৭৯৯৬১", "email" => "sadiasagor1993@ygmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "পিরোজপুর সদর", "name" => "সাগর সিকদার", "phone" => "০১৭৪৬৬৬৮১০৮", "email" => "sikder.sagar@yahoo.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "পিরোজপুর সদর", "name" => "অর্পনা অধিকারী", "phone" => "০১৭০০৯৯৩৭৪৮", "email" => "aparna487@yahoo.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "পিরোজপুর সদর", "name" => "মোঃ আব্দুল ওহাব খান", "phone" => "০১৭১০১৮১৯৮৯", "email" => "khanohab77.chal@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "পিরোজপুর সদর", "name" => "রম্নবেল শেখ", "phone" => "০১৭৩৪১৭৭৭২০", "email" => "rubel.rs59@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "পিরোজপুর সদর", "name" => "ডালিয়া আক্তার", "phone" => "০১৭৮২৯২৯৫২৭", "email" => "daleyashamin@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "পিরোজপুর সদর", "name" => "মোঃ ফজলুল করিম", "phone" => "০১৭২৭৭৭৯৭১৪", "email" => "fkshamim111@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "পিরোজপুর সদর", "name" => "হাফিজা আক্তার", "phone" => "০১৭৮৮৭০৯৬৪২", "email" => "hafizaakter094@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "পিরোজপুর সদর", "name" => "মোঃ তরিফুল ইসলাম", "phone" => "০১৭৬৮৯৮৭৩৩৯", "email" => "tarikultonun@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "পিরোজপুর সদর", "name" => "মোঃ কামরুজ্জামান", "phone" => "০১৭১৭২৪৯০৬৮", "email" => "ppourashava@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "পিরোজপুর সদর", "name" => "মিতু আক্তার", "phone" => "০১৯৩৪৩১৫৭২৫", "email" => "mituakther2323@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "পিরোজপুর", "name" => "আরিফ খান", "phone" => "০১৭৩২৫৩৪৫৪৭", "email" => "zpdcpirojpur@gmail.com"],

			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "নাজিরপুর", "name" => "মোঃ নাজমুল হোসাইন", "phone" => "০১৮৩১৩৮৬৪৯৭", "email" => "nazmulhossain124@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "নাজিরপুর", "name" => "মানছুরা আক্তার", "phone" => "০১৭৬৪০৭৩৫৫২", "email" => "mansuraakter@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "নাজিরপুর", "name" => "মিলন মন্ডল", "phone" => "০১৭০৬৩৪৬৪৬৬", "email" => "milonmandal2@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "নাজিরপুর", "name" => "ইতি বাকচী", "phone" => "০১৭৩৩৬০৮৬৮৯", "email" => "etibagchi8@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "নাজিরপুর", "name" => "সুকামত্ম মন্ডল", "phone" => "০১৭৩৩১০১৬৭৭", "email" => "sukantamondal121@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "নাজিরপুর", "name" => "পাপড়ি মন্ডল", "phone" => "০১৭৫২৪৮২৫৩৯", "email" => "paprimondal1984@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "নাজিরপুর", "name" => "আকাশ বেপরী", "phone" => "০১৭৪২০৭৮৪২", "email" => "akashbepari409@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "নাজিরপুর", "name" => "সাবিনা খানম", "phone" => "০১৭৮৫৬৯৭৯৬০", "email" => "sabinakhanam433@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "নাজিরপুর", "name" => "এনামুল হক মুক্তা", "phone" => "০১৭১১৯০০৭৩৬", "email" => "muktasarder@yahoo.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "নাজিরপুর", "name" => "সোনিয়া আক্তার", "phone" => "০১৭৭০৫০০৪৬৭", "email" => "soniaakter859@yahoo.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "নাজিরপুর", "name" => "সুব্রত মন্ডল", "phone" => "০১৭৫৬২৯৫২৭৭", "email" => "subratam061@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "নাজিরপুর", "name" => "শংকরী সিকদার", "phone" => "০১৭৫৩৯৮৪৩৬৯", "email" => "sankarisikder69@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "নাজিরপুর", "name" => "সোনিয়া আক্তার", "phone" => "০১৭৬৫৮৬৭৯২৮", "email" => "sonialaz1991@gmail.com"],
			["division" => "বরিশাল", "dist" => "পিরোজপুর", "sub_dist" => "নাজিরপুর", "name" => "মোঃ আশিকুল ইসলাম", "phone" => "০১৭৪৩৫৬৪৫২০", "email" => "mdashikulislam80@gmail.com"]
		];

		$trans	= ["১"=>"1", "২"=>"2", "৩"=>"3", "৪"=>"4", "৫"=>"5", "৬"=>"6", "৭"=>"7", "৮"=>"8", "৯"=>"9", "০"=>"0", "o"=>"0", "O"=>"0"];

		for ($i=0; $i < count($data); $i++) {
			$list['division'] 		= $data[$i]['division'];
			$list['dist'] 			= $data[$i]['dist'];
			$list['sub_dist'] 		= $data[$i]['sub_dist'];
			$list['name'] 			= $data[$i]['name'];
			$list['phone'] 			= strtr($data[$i]['phone'], $trans);
			$list['email'] 			= $data[$i]['email'];
			$list['created_at'] 	= \Carbon\Carbon::now();

			//My_model::insert('entrepreneurs_list', $list);
		}
			return "It's Completed";
   }
}