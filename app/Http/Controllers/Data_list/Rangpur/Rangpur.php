<?php

namespace App\Http\Controllers\Data_list\Rangpur;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\My_model;

class Rangpur extends Controller
{
   public function index()
   {
   		$data = [
   			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "বদরগঞ্জ", "name" => "মোঃমাহাফুজারর রহমান", "phone" => "০১৭৩৫৯৮১৩৬১", "email" => "kalupara13@gfmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "বদরগঞ্জ", "name" => "মোঃ মোবারক হোসেন", "phone" => "০১৭৮৬৯৭৩১৮৮", "email" => "bishnupur.uisc@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "বদরগঞ্জ", "name" => "মোঃ মোজাহারুল ইসলাম", "phone" => "০১৭১৩৭০৯৪৬১", "email" => "mozaharul1975@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "বদরগঞ্জ", "name" => "মোছাঃ রোকেয়া বেগম	", "phone" => "০১১৯৫৩৫৬৭৭১", "email" => "damodarpurup@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "বদরগঞ্জ", "name" => "মোঃ এনামুল হক", "phone" => "01738552386",	"email" => "enamulhaque32@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "বদরগঞ্জ", "name" => "মোছাঃ শারমিন আক্তার ", "phone" => "01744618160",	"email" => "akter19789@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "বদরগঞ্জ", "name" => "মোঃ মমিনুর রহমান", "phone" => "০১৭৩৫১৯১১৫০", "email" => "mominrahman96@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "বদরগঞ্জ", "name" => "আহসান হাবিব", "phone" => "০১৭২৩৬৯৫৩৯৬", "email" => "habibbd9@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "বদরগঞ্জ", "name" => "মোঃ ওবায়দুল হক", "phone" => "০১৭১৩৭৭০৬৭৯", "email" => "obaidulhaque611@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "বদরগঞ্জ", "name" => "মো: মোস্তাহিদুল ইসলাম (মুন্না)", "phone" => "01718910697",	"email" => "mostahidulmunna@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "বদরগঞ্জ", "name" => "মোছাঃ আতোয়ারা খাতুন", "phone" => "০১৭৬১০৭৪৩৩৩", "email" => "atoyara1990@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "বদরগঞ্জ", "name" => "মো: শাহনেওয়াজ আলী", "phone" => "০১৭৪৩৩৯২৮৩৯", "email" => "shanaousmondol1992@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "বদরগঞ্জ", "name" => "মোঃ মমিনুল ইসলাম", "phone" => "০১৭৫৮০৯৬৫৭১", "email" => "momin.islam69@gmail.com"],

			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "গংগাচড়া", "name" => "মোঃ শাহীনুর রহমান", "phone" => "01723974796",	"email" => "shahinur2012@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "গংগাচড়া", "name" => "নাজমুন্নাহার ", "phone" => "০১৭৯১৮২৬৯৫৮", "email" => "shahinur2012@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "গংগাচড়া", "name" => "মোঃ হাসানুজ্জামান জুয়েল", "phone" => "01718864755",	"email" => "juel1983@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "গংগাচড়া", "name" => "মোছাঃ ফারজানা বেগম", "phone" => "01730854592",	"email" => "juel.ran@yahoo.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "গংগাচড়া", "name" => "Md. Abdul Bari", "phone" => "01740929336",	"email" => "bari10131989@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "গংগাচড়া", "name" => "ferdoushi akther", "phone" => "01745546731",	"email" => "ferdoushi01211979@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "গংগাচড়া", "name" => "মোছাঃ ফেরদৌসি আক্তার ", "phone" => "০১৭৪৫৫৪৬৭৩১", "email" => "resma8101989@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "গংগাচড়া", "name" => "মোঃ এমদাদুল হক", "phone" => "01745085038",	"email" => "emdad.uisc@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "গংগাচড়া", "name" => "মোছাঃ মাসুমা আক্তার ", "phone" => "01773165291",	"email" => "masuma.udc@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "গংগাচড়া", "name" => "	মোঃ মাহফুজার রহমান", "phone" => "০১৭৩৪৮৮৩৭২২", "email" => "mahafuzar2011@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "গংগাচড়া", "name" => "মোছাঃ সুলতানা খাতুন", "phone" => "01739731116",	"email" => "mahfuzar2011@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "গংগাচড়া", "name" => "মোঃ মাহফুজার রহমান মিলন", "phone" => "01767511512",	"email" => "milon595@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "গংগাচড়া", "name" => "মোঃ মমিনুল ইসলাম বাবু", "phone" => "০১৮২৩০১৭০৮", "email" => "babumia1980@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "গংগাচড়া", "name" => "রোকছেনা বেগম", "phone" => "01761370187",	"email" => "jhilik2005@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "গংগাচড়া", "name" => "সুকদেব রায়", "phone" => "01773074232",	"email" => "nohaliup.rang@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "গংগাচড়া", "name" => "মোঃ শামসুজ্জামান", "phone" => "01721119350",	"email" => "babu30061989@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "গংগাচড়া", "name" => "মোছাঃ রাজিয়া সুলতানা", "phone" => "01746604753",	"email" => "babu30061989@gmail.com"],

			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "কাউনিয়া সদর", "name" => "মোছাঃ মুনমুন নাহার।", "phone" => "০১৭৬৭০৩৭১৯২", "email" => "munmunbest7@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "কাউনিয়া সদর", "name" => "মোঃ মোশরেকুল ইসলাম", "phone" => "01717617856",	"email" => "monwelcome@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "কাউনিয়া সদর", "name" => "মো: আজাহারুল ইসলাম", "phone" => "০১৭৩১৮৯৬১৬৫", "email" => "azaharulislam40@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "কাউনিয়া সদর", "name" => "মো: দেলোয়ার হোসাইন জিয়া", "phone" => "০১৭২৬৮২৫৯৮৮", "email" => "md.deloarhassainzia@yahoo.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "কাউনিয়া সদর", "name" => "Tupan chandra sharkar", "phone" => "01737894411",	"email" => "saraiup.rang@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "কাউনিয়া সদর", "name" => "Mst. Nazma Begum", "phone" => "01775517205",	"email" => "saraiup.rang@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "কাউনিয়া সদর", "name" => "মো: ইউনুছ আলী", "phone" => "০১৭৪৬৩১২১৪৪", "email" => "unus19890@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "কাউনিয়া সদর", "name" => "মোছা: ফাতেমা জান্নাত", "phone" => "০১৭৮৪০০৯৭১৪", "email" => "fatama1995a@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "কাউনিয়া সদর", "name" => "MD:ALTAF HOSSAIN ", "phone" => "০১৭৩৪৫৩১৫২৫", "email" => "onindobegum@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "কাউনিয়া সদর", "name" => "মোছাঃসম্পা বেগম ", "phone" => "01748131781",	"email" => "onindobegum@gmail.com"],

			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "রংপুর সদর", "name" => "মোঃ রফিকুল ইসলাম", "phone" => "০১৭৩৮১৬৯১৮৪", "email" => "chandonpat@yahoo.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "রংপুর সদর", "name" => "আরিফা আক্তার", "phone" => "০১৭৮০৯০৩৩২১", "email" => "chandanpatup.rang@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "রংপুর সদর", "name" => "লাভলী রানী", "phone" => "০১৭৩৮১৭০১৭", "email" => "lovelymohanto_2021@yahoo.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "রংপুর সদর", "name" => "মোঃ মোখলেছার রহমান", "phone" => "০১৭৭২৮৫৮৩৪০", "email" => "lovelymohanto-2021@yahoo.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "রংপুর সদর", "name" => "মোঃ আনোয়ার সাদাত", "phone" => "01740975358",	"email" => "asadat52@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "রংপুর সদর", "name" => "মোাছা:ইসমত আরা বেগম", "phone" => "০১৭৮০৬৬৩৮৬১", "email" => "asadat52@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "রংপুর সদর", "name" => "নবদ্বীপ চন্দ্র সরকার", "phone" => "০১৭১৯২৫৭০১৫", "email" => "nabadwip1980@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "রংপুর সদর", "name" => "আমরিন আক্তার ", "phone" => "০১৭৫০৯৮৪৫১৪", "email" => "nabadwip1980@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "রংপুর সদর", "name" => "মোঃ আরিফুজ্জামান মুন", "phone" => "01722668194",	"email" => "moonarif94@yahoo.com"],

			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "মিঠাপুকুর", "name" => "মোছাঃ মৌসুমী বেগম", "phone" => "০১৭৪৪৯৩২২১৯", "email" => "sadwapuskoroniup.rang@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "মিঠাপুকুর", "name" => "মোছাঃ জাকিয়া", "phone" => "01797628933",	"email" => "jakia001991@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "মিঠাপুকুর", "name" => "মোঃ রাসেল মিয়া", "phone" => "01738037552",	"email" => "rashelmiah_19@yahoo.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "মিঠাপুকুর", "name" => "মো: সোহেল রানা ", "phone" => "01740055366",	"email" => "ranabil1991@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "মিঠাপুকুর", "name" => "মোছা: আরজিনা বেগম ", "phone" => "০১৭৫০২১৫৩৪৫", "email" => "ranabil1991@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "মিঠাপুকুর", "name" => "মোঃ নুর -এ- হাবীব রানা", "phone" => "০১৭১৯২৪৬৪৪২", "email" => "rana_habib_bd@yahoo.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "মিঠাপুকুর", "name" => "SHIRINA BEGUM", "phone" => "01786949718",	"email" => "shirinsheraz@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "মিঠাপুকুর", "name" => "মোঃ মাইদুল ইসলাম", "phone" => "01723846255",	"email" => "maidulislam55@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "মিঠাপুকুর", "name" => "মোছাঃ পিপুলী খাতুন", "phone" => "01751403377",	"email" => "maidulislam55@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "মিঠাপুকুর", "name" => "মোঃ আসাদুজ্জামান", "phone" => "০১৭৮৮১৫৭৪৩৮", "email" => "ttc.asad@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "মিঠাপুকুর", "name" => "মোছাঃ ফেন্সী বেগম", "phone" => "01762770967",	"email" => "ttc.asad@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "মিঠাপুকুর", "name" => "Mis. Fatiha ", "phone" => "০১৭৪২১৯৫৪৯৫", "email" => "fatiha0000@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "মিঠাপুকুর", "name" => "মো: রবিউল ইসলাম", "phone" => "০১৭২৩৮৪৬৩০৬", "email" => "milarhamauisc@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "মিঠাপুকুর", "name" => "মোছা: রেনুফা নাজনীন", "phone" => "০১৭৭৫৩৭১৪০৭", "email" => "renufa07@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "মিঠাপুকুর", "name" => "মোঃ মোখলেছার রহমান", "phone" => "০১৭৩৮৫০৪৫৫৪", "email" => "moklesarrahman@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "মিঠাপুকুর", "name" => "মোছাঃ লাকী আক্তার", "phone" => "০১৭৪৪৯৭৮৩৯৪", "email" => "luckyaktar1996@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "মিঠাপুকুর", "name" => "সুবোধ কুমার মহন্ত", "phone" => "০১৭২৮৮৬৩৫২২", "email" => "shuvomultimedia@yahoo.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "মিঠাপুকুর", "name" => "মোছাঃ মৌসুমি আাক্তার", "phone" => "০১৭২৪১৪১৩০১", "email" => "archonashuvo@yahoo.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "মিঠাপুকুর", "name" => "মো: আরিফুল ইসলাম	", "phone" => "01744608846","email" => "ariful_uisc@yahoo.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "মিঠাপুকুর", "name" => "মোছা: শান্তনা বেগম	", "phone" => "01723-249428", "email" => "ariful_uisc@yahoo.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "মিঠাপুকুর", "name" => "মোঃ রমজান আলী", "phone" => "01737551766",	"email" => "ramjanudc@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "মিঠাপুকুর", "name" => "মোছাঃ তৌহিদা নাসরিন", "phone" => "০১৭৩১৬২২১১৫", "email" => "tauhidanasrin@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "মিঠাপুকুর", "name" => "মোঃ খায়রুল ইসলাম", "phone" => "০১৭২৩৬৪৮০৯১", "email" => "khairulislam68@yahoo.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "মিঠাপুকুর", "name" => "মোছাঃ লাবণী আক্তার", "phone" => "01723151125",	"email" => "khairulislam68@yahoo.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "মিঠাপুকুর", "name" => "মোঃ নুর আলী", "phone" => "০১৭৯০৪৩২১১০", "email" => "nurali818@yahoo.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "মিঠাপুকুর", "name" => "মোছাঃ রুনা আক্তার", "phone" => "01788665348",	"email" => "runaakter9612@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "মিঠাপুকুর", "name" => "মো: রোকনুরজ্জামান রনি", "phone" => "০১৭৩৮১৪৫৪২৩", "email" => "ronygopalpur88@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "মিঠাপুকুর", "name" => "মোছা: স্বপ্না বেগম ", "phone" => "01717223939",	"email" => "ronygopalpur88@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "মিঠাপুকুর", "name" => "জি, এম, আবু সুফিয়ান", "phone" => "০১৭২৩৬৪৭৭৮৪", "email" => "gm_sufian@yahoo.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "মিঠাপুকুর", "name" => "মোছাঃ কানিজ ফাতেমা", "phone" => "01738255273",	"email" => "gsufian@ymail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "মিঠাপুকুর", "name" => "Zitu Ahmed Rubel", "phone" => "01723279332",	"email" => "zituahmed1991@yahoo.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "মিঠাপুকুর", "name" => "Nargis Parvin", "phone" => "01723279332",	"email" => "zituahmed1991@yahoo.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "মিঠাপুকুর", "name" => "মোহাম্মদ আলী মিয়া", "phone" => "01736360070",	"email" => "mahammadali12011976@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "মিঠাপুকুর", "name" => "মোছাঃ বিলকিছ বেগম", "phone" => "01750546441",	"email" => "eleyasscout2012@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "মিঠাপুকুর", "name" => "Md. Masud Rana", "phone" => "01723275188",	"email" => "masudrana275188@yahoo.com"],

			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগাছা", "name" => "Most. Rezina Akhtar", "phone" => "01723275188",	"email" => "masudrana275188@yahoo.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগাছা", "name" => "মো: গোলাম জাকারিয়া", "phone" => "01739254669",	"email" => "gjakaria84@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগাছা", "name" => "মোঃ জিয়াউর রহমান", "phone" => "০১৭২২৮০৭০৮৭", "email" => "ziaur.pirgacha1981@yahoo.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগাছা", "name" => "মোছাঃ লাভলী আক্তার ", "phone" => "01739862997",	"email" => "ziaur.pirgacha1981@yahoo.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগাছা", "name" => "Md. asaduzzaman", "phone" => "01737936369",	"email" => "asaduzzaman2030@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগাছা", "name" => "Mst. Zorina Begum", "phone" => "01788281451",	"email" => "asaduzzaman2030@gmil.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগাছা", "name" => "মোঃ লেবু মিয়া", "phone" => "01727985548",	"email" => "labukaikuri@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগাছা", "name" => "মোছাঃ নারগীছ আক্তার", "phone" => "01729536247",	"email" => "uisc555@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগাছা", "name" => "মোঃ রফিকুল ইসলাম", "phone" => "01729734613",	"email" => "rafiq.rangpur@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগাছা", "name" => "মোছাঃ নাজমা আক্তার", "phone" => "01779445894",	"email" => "rangpurnazma@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগাছা", "name" => "মোঃ আতাউর রহমান (মিঠু)", "phone" => "01716926343",	"email" => "ataur811@yahoo.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগাছা", "name" => "মোছাঃ অারেফা আক্তার বিথী", "phone" => "০১৮২৪৪৭৫৭৩৪", "email" => "arafa9555@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগাছা", "name" => "মোঃ আশিকুর রহমান আকন্দ", "phone" => "01774545995",	"email" => "asikurrepon@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগাছা", "name" => "মোঃ মতিয়ার রহমান", "phone" => "01716320375",	"email" => "mdmatiarbd@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগাছা", "name" => "মোছাঃ সামিরা মোস্তারী", "phone" => "01797575587",	"email" => "mostari587@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগাছা", "name" => "আফসানা খাতুন ", "phone" => "01733148287",	"email" => "afsana8287@gmail.com"], 
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগাছা", "name" => "মোঃ ওয়াদু মনি", "phone" => "01689795063", "email" => "mony223366@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগাছা", "name" => "মোছাঃ কোহিনুর খাতুন", "phone" => "01755419106",	"email" => "mony223366@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগাছা", "name" => "মোঃ আবু তাহের", "phone" => "01758585525",	"email" => "abutaher5525@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগাছা", "name" => "মোছাঃ মারুফা ইয়াসমিন", "phone" => "01761262624",	"email" => "yesminmarufa70@gmail.com"],
			
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগঞ্জ", "name" => "সীমা রাণী", "phone" => "01728679046",	"email" => "kumarisimarani@yahoo.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগঞ্জ", "name" => "সুমন কুমার মহন্ত", "phone" => "০১৭১৯৪৬৩২৭৫", "email" => "sreesumon9@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগঞ্জ", "name" => "মোঃ বাকিবিল্লাহ আল হাসান ", "phone" => "০১৭১০৭২৭৬২১", "email" => "bakibillhaallhasan1980@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগঞ্জ", "name" => "মোছাঃ তাহসিনা খাতুন", "phone" => "০১৭৯৬৮০২৬৭৮", "email" => "bakibillhaallhasan1980@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগঞ্জ", "name" => "মো: নুর হাবিব পারভেজ ফুল", "phone" => "০১৭৩২১৬৬৭৭০", "email" => "chatraup.rang@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগঞ্জ", "name" => "মোছা: তহমিনা বেগম", "phone" => "০১৭৭০৩৭৫৬৮০", "email" => "chatraup.rang@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগঞ্জ", "name" => "মো:- আসিব আহম্মেদ", "phone" => "০১৭৪০৪৪০০৬৬", "email" => "asib.ahammed@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগঞ্জ", "name" => "মোঃ মনোয়ার হোসেন", "phone" => "০১৭৮৪৫৮১৭৬৭", "email" => "monowarhossen1970@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগঞ্জ", "name" => "মোছাঃ শাপলা খাতুন", "phone" => "০১৭৫০৮৫৩৬১৭", "email" => "shaplak49@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগঞ্জ", "name" => "মো: রেজাউল করিম", "phone" => "01737720921",	"email" => "rezaulkrim877@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগঞ্জ", "name" => "Most. Mousomi khatun", "phone" => "01744574256",	"email" => "Rezaulkrim877@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগঞ্জ", "name" => "মো: সুলতান মাহমুদ আকন্দ", "phone" => "০১৭৩৭৫৮১১০৯", "email" => "mahmudsultan79@yahoo.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগঞ্জ", "name" => "মোছা: আয়শা ছিদ্দিকা মেঘলা", "phone" => "০১৭২৫৭৪৭৮৮৬", "email" => "bangladeshpr1971@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগঞ্জ", "name" => "মো: রাজু মন্ডল", "phone" => "01719246355",	"email" => "bangladeshpr1971@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগঞ্জ", "name" => "মোছা: আছিয়া খাতুন", "phone" => "০১৫৫৭২১৭৬৮০", "email" => "bangladeshpr1971@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগঞ্জ", "name" => "মোঃ রবিউল আলম", "phone" => "01723271463",	"email" => "rabiulalam1985@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগঞ্জ", "name" => "মোছাঃ রেহেনা বেগম", "phone" => "01761342226",	"email" => "rabiulalam1985@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগঞ্জ", "name" => "মোঃ হেলাল মিয়া", "phone" => "01737524979",	"email" => "helalkhan1991@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগঞ্জ", "name" => "মোছাঃ আফরোজা আক্তার", "phone" => "০১৭৩৭৫২৪৯৭৯", "email" => "helalkhan1991@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগঞ্জ", "name" => "মোঃ শরিফুল ইসলাম ", "phone" => "০১৭৫১২০৫১৫২", "email" => "shariful2021@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগঞ্জ", "name" => "Md. Shariful Islam", "phone" => "01751205152",	"email" => "shariful2021@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগঞ্জ", "name" => "উম্মে কুলছুমা খাতুন", "phone" => "০১৭৩৯১৬০৭৪৩", "email" => "shariful2021@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগঞ্জ", "name" => "মোঃ শরিফুল ইসলাম ", "phone" => "০১৭৫১২০৫১৫২", "email" => "shariful2021@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগঞ্জ", "name" => "মো: মোফাজ্জল হোসেন", "phone" => "01738560200",	"email" => "kholahati2011@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগঞ্জ", "name" => "মোছাঃ তানজিরা বেগম", "phone" => "০১৭৩৮৫৬০২০০", "email" => "kholahati2011@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগঞ্জ", "name" => "মোঃ শরিফুল আলম", "phone" => "০১৭১৭৫২৪৫১৩", "email" => "shariful_alom_r@yahoo.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগঞ্জ", "name" => "মো: ফাইজুর রহমান", "phone" => "০১৭৬৮৮২৬১৭৬", "email" => "mdfizurrahman1995uisc@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "পীরগঞ্জ", "name" => "মোছা: আইরিন খাতুন", "phone" => "01737597107",	"email" => "mdfizurrahman1995uisc@gmail.com"],

			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "তারাগঞ্জ", "name" => "মোঃ আইয়ুব আলী", "phone" => "01751432348",	"email" => "ayubali345@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "তারাগঞ্জ", "name" => "মোছাঃ আয়শা ছিদ্দিকা", "phone" => "০১৯৩৭৩৭৯৪২৮", "email" => "ayubali345@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "তারাগঞ্জ", "name" => "মোঃ আখতারুজ্জামান", "phone" => "01740947562",	"email" => "sobuz1983@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "তারাগঞ্জ", "name" => "মেরিনা খাতুন", "phone" => "০১৭১০৪৮৮৩৭৩", "email" => "merina161992@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "তারাগঞ্জ", "name" => "মানিক চন্দ্র শীল", "phone" => "01740053000",	"email" => "manik.rangpurbd@yahoo.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "তারাগঞ্জ", "name" => "Most. Aktari Khanom", "phone" => "01735938487",	"email" => "aktarikhanom@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "তারাগঞ্জ", "name" => "মো: মোয়াজ্জেম হোসেন ", "phone" => "০১৭২৮০৮৮৪৮৮", "email" => "moazzemhossen88@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "তারাগঞ্জ", "name" => "মোছা: ফেরদৌসী আক্তার ", "phone" => "01751461810",	"email" => "uisc.ferdowsi@gmail.com"],
			["division" => "রংপুর", "dist" => "রংপুর", "sub_dist" => "তারাগঞ্জ", "name" => "জান্নাতুল ফেরদৌস", "phone" => "০১৭৪০১৮৬৬৭৭", "email" => "alamin.mukul1982@gmail.com"]
   		];

		$trans	= ["১"=>"1", "২"=>"2", "৩"=>"3", "৪"=>"4", "৫"=>"5", "৬"=>"6", "৭"=>"7", "৮"=>"8", "৯"=>"9", "০"=>"0", "o"=>"0", "O"=>"0"];

		for ($i=0; $i < count($data); $i++) {
			$list['division'] 		= $data[$i]['division'];
			$list['dist'] 			= $data[$i]['dist'];
			$list['sub_dist'] 		= $data[$i]['sub_dist'];
			$list['name'] 			= $data[$i]['name'];
			$list['phone'] 			= strtr($data[$i]['phone'], $trans);
			$list['email'] 			= $data[$i]['email'];
			$list['created_at'] 	= \Carbon\Carbon::now();

			//My_model::insert('entrepreneurs_list', $list);

		}
			return "It's Completed";
   }
}