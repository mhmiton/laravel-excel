<?php

namespace App\Http\Controllers\Data_list\Rangpur;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\My_model;

class Thakurgaon extends Controller
{
   public function index()
   {
   		$data = [

			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "ঠাকুরগাঁও সদর", "name" => "মোঃ ফারুক হোসেন", "email" => "ruheaudc1no@gmail.com", "phone" => "০১৭১৫৬৭২২২২"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "ঠাকুরগাঁও সদর", "name" => "তামান্না আক্তার মুন্নি", "email" => "aktertamanna291@gmail.com", "phone" => "০১৭৯৭৮৮৫০৮৮"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "ঠাকুরগাঁও সদর", "name" => "নরেশ চন্দ্র বর্মন", "email" => "nareshak2@gmail.com", "phone" => "০১৭৭০৩৯২৫৭৮"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "ঠাকুরগাঁও সদর", "name" => "মোঃ আমিরুল ইসলাম", "email" => "amirulak2@gmail.com", "phone" => "০১৭৫১০২৯৩৩৪"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "ঠাকুরগাঁও সদর", "name" => "মোঃ সোহেল রানা", "email" => "uisc3noakcha@yahoo.com", "phone" => "০১৭৩৭৪২৪৯৮৭"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "ঠাকুরগাঁও সদর", "name" => "মোছাঃ কিসমতারা বেগম", "email" => "uisc3noakcha@yahoo.com", "phone" => "০১৭১০৪২৭৭৫৬"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "ঠাকুরগাঁও সদর", "name" => "মোঃ দবিরুল ইসলাম", "email" => "uisc4noborogaon@yahoo.com", "phone" => "০১৭৩৩৭৬৭৫১৭"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "ঠাকুরগাঁও সদর", "name" => "শরৎ চন্দ্র বর্মন", "email" => "uisc4noborogaon@yahoo.com", "phone" => "০১৭৪৪৩৩২৭৭১"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "ঠাকুরগাঁও সদর", "name" => "মোঃ জিয়াউর রহমান", "email" => "uiscbalia577@gmail.com", "phone" => "০১৭৪৫০৮২৩৪১"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "ঠাকুরগাঁও সদর", "name" => "মোঃ উজ্জল হক", "email" => "udcuzzalbalia@gmail.com", "phone" => "০১৭১৭১০৭৫৮০"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "ঠাকুরগাঁও সদর", "name" => "শ্রী মানিক চন্দ্র রায়", "email" => "manik01764709230@yahoo.com", "phone" => "০১৭৬৪৭০৯২৩০"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "ঠাকুরগাঁও সদর", "name" => "সোনালী রাণী", "email" => "uisct01764709230@gmail.com", "phone" => "০১৭৩৭৩৮০৪৯৬"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "ঠাকুরগাঁও সদর", "name" => "মোঃ এম এ মাসুদ রানা প্রধান", "email" => "masudranaw.bss@gmail.com", "phone" => "০১৭৩৭৪৭০২৪২"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "ঠাকুরগাঁও সদর", "name" => "মোছাঃ শিরীন আখতার", "email" => "chilaronguisc@yahoo.com", "phone" => "০১৭৬১৬১৭২১৯"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "ঠাকুরগাঁও সদর", "name" => "মোঃ আমিরুল ইসলাম", "email" => "uiscrahimanpur@yahoo.com", "phone" => "০১৭৩৯৮৩৪৭১৯"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "ঠাকুরগাঁও সদর", "name" => "মোঃ ওয়ালিউর সুলতান", "email" => "tipuwst@gmail.com", "phone" => "০১৭১৯৭১০১৭২"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "ঠাকুরগাঁও সদর", "name" => "মোঃ মানিক আহম্মেদ", "email" => "raipuruisc@yahoo.com", "phone" => "০১৭৪৪৬৩২৭৭৯"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "ঠাকুরগাঁও সদর", "name" => "মোঃ মিজানুর রহমান", "email" => "mdmizanurrahman940@yahoo.com", "phone" => "০১৭২২৭২৯১৪৮"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "ঠাকুরগাঁও সদর", "name" => "মোঃ রুস্তম খাঁন", "email" => "rustomkhan188@yahoo.com", "phone" => "০১৭৩১৩২৮২৮৩"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "ঠাকুরগাঁও সদর", "name" => "মোঃ আবু বক্কর সিদ্দীক", "email" => "bakkarsiddik89@gmail.com", "phone" => "০১৭৩৭৮২৩৮৭৩"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "ঠাকুরগাঁও সদর", "name" => "মোছাঃ মর্জিনা আক্তার", "email" => "marjinaaktermilune@gmail.com", "phone" => "০১৭৯৮৫৯৫৪৯১"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "ঠাকুরগাঁও সদর", "name" => "মোছাঃ ফজিলা খাতুন", "email" => "fazila1989@gmail.com", "phone" => "০১৭৪৪৫৬৫৪৭৬"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "ঠাকুরগাঁও সদর", "name" => "মোঃ ফরহাত ইসলাম রতন", "email" => "ratontkg@gmail.com", "phone" => "০১৭৩৭৯৮৮২২৪"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "ঠাকুরগাঁও সদর", "name" => "মোঃ জুয়েল ইসলাম", "email" => "udcgoreya13@gmail.com", "phone" => "০১৭২২১৫৯৪০২"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "ঠাকুরগাঁও সদর", "name" => "কুমদ চন্দ্র রায়", "email" => "kumodroy7@gmail.com", "phone" => "০১৭৮৫২১৯৫৬২"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "ঠাকুরগাঁও সদর", "name" => "মোঃ শাহজালাল", "email" => "shahjalaludc@gmail.com", "phone" => "০১৭৩৭৫২৭৬৬৯"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "ঠাকুরগাঁও সদর", "name" => "মোছাঃ খাদিজা বেগম", "email" => "kathijakhatun@yahoo.com", "phone" => "০১৭৭৪০৩৬৯০১"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "ঠাকুরগাঁও সদর", "name" => "এ কে এম শহিদুল্লাহ", "email" => "dabipurno15uisc@gmail.com", "phone" => "০১৭১৩৭৮০৫৮৩"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "ঠাকুরগাঁও সদর", "name" => "মোছাঃ খাদিজা বেগম", "email" => "mstrina1341972@gmail.com", "phone" => "০১৭১৫১৩৮২১৫"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "ঠাকুরগাঁও সদর", "name" => "মোঃ ফজলে এলাহী", "email" => "fazlaalahi750@gmail.com", "phone" => "০১৭৬১৩০১৬৩৫"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "ঠাকুরগাঁও সদর", "name" => "মোঃ রাজু রায়হান", "email" => "razu.royhan@yahoo.com", "phone" => "০১৭৯৮৯০২৮৭৭"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "ঠাকুরগাঁও সদর", "name" => "বিপুল চন্দ্র রায়", "email" => "jagannathpurup17@gmail.com", "phone" => "০১৭৩৮১১৯৯৪৬"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "ঠাকুরগাঁও সদর", "name" => "মোছাঃ হুসনে আরা আক্তার", "email" => "jagannathpurudc@gmail.com", "phone" => "০১৭১১৭৩০৫৩৫"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "ঠাকুরগাঁও সদর", "name" => "জগদীশ রায়", "email" => "shukhanpukuri@gmail.com", "phone" => "০১৭৩৮৫৮৫১৬১"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "ঠাকুরগাঁও সদর", "name" => "প্রতিমা রাণী", "email" => "bd1union8digital.center@gmail.com", "phone" => "০১৭৯৭৯৩৯৪৯১"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "ঠাকুরগাঁও সদর", "name" => "মোঃ এরশাদ উল্লাহ", "email" => "begunbariuisc@yahoo.com", "phone" => "০১৭২৯৬১৬৯৯৫"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "ঠাকুরগাঁও সদর", "name" => "শিল্পী রাণী", "email" => "shilpirani2020@gmail.com", "phone" => "০১৭৫০৫১৬৯০৭"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "ঠাকুরগাঁও সদর", "name" => "মোঃ আব্দুল জব্বার", "email" => "majabbar506@gmail.com", "phone" => "০১৮২৮১৪৮৫০৬"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "ঠাকুরগাঁও সদর", "name" => "অর্চনা সেন", "email" => "archanasen@gmail.com", "phone" => "০১৭৭৪৩৭২০৬০"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "ঠাকুরগাঁও সদর", "name" => "নারায়ন চন্দ্র বর্মন", "email" => "narayanbarman@gmail.com", "phone" => "০১৭২১০০৩৩৫৯"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "ঠাকুরগাঁও সদর", "name" => "পপি রাণী সেন", "email" => "popirani@gmail.com", "phone" => "০১৭১৯০০১১১২"],

			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "বালিয়াডাঙ্গী", "name" => "সুপেন চন্দ্র", "email" => "supenchandra1993@gmail.com", "phone" => "০১৭৬৪৮৪২৬৯০"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "বালিয়াডাঙ্গী", "name" => "মোছাঃ পারভীন আক্তার", "email" => "parvin1989@gmail.com", "phone" => "০১৭৩২২২৭৩২০"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "বালিয়াডাঙ্গী", "name" => "মোঃ আশরাফুল ইসলাম", "email" => "asrafulsultan@gmail.com", "phone" => "০১৭৭৪৫১১১০৪"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "বালিয়াডাঙ্গী", "name" => "মোছাঃ রুপসানা আক্তার", "email" => "rupsanareyad@gmail.com", "phone" => "০১৭৯১৫৭৮৫৯৯"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "বালিয়াডাঙ্গী", "name" => "মোঃ আব্দুল হাকিম", "email" => "uischakim@yahoo.com", "phone" => "০১৭৩৮৬৪৮১১২"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "বালিয়াডাঙ্গী", "name" => "মোছাঃ বিউটি বেগম", "email" => "dhontolaup3@gmail.com", "phone" => "০১৭৪০২৭৯৩৯৪"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "বালিয়াডাঙ্গী", "name" => "মোঃ হারুন অর রশিদ", "email" => "boropolashbari1@gmail.com", "phone" => "০১৭১৩৭৯৮১১৯"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "বালিয়াডাঙ্গী", "name" => "মোঃ সোহেল রানা", "email" => "sumon.ecen@gmail.com", "phone" => "০১৭৩৭৮৩৯১০২"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "বালিয়াডাঙ্গী", "name" => "মোঃ নবাব আজম", "email" => "nababajam@gmail.com", "phone" => "০১৭৩২৯৯১০০৭"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "বালিয়াডাঙ্গী", "name" => "মোছাঃ সাথী আকতার", "email" => "nababajam@gmail.com", "phone" => "০১৭৩৭৩৩৪১০৪"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "বালিয়াডাঙ্গী", "name" => "মোঃ আইজুদ্দিন", "email" => "ayzuddin02@gmail.com", "phone" => "০১৭৫১০০৬৮০১"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "বালিয়াডাঙ্গী", "name" => "শ্রী মতি মেনুকা রাণী", "email" => "manukarani56@gmail.com", "phone" => "০১৭৫০২৪৫২৫৬"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "বালিয়াডাঙ্গী", "name" => "মোঃ রুবেল রানা", "email" => "rubelajam1@gmail.com", "phone" => "০১৭৩৭০৫০৪৩৫"],

			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "পীরগঞ্জ", "name" => "মোঃ জুয়েল রানা", "email" => "mdjuwelrana61@gmail.com", "phone" => "০১৭৪০১০২৫৬১"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "পীরগঞ্জ", "name" => "মোছাঃ তহুরা খাতুন", "email" => "msttohura385@gmail.com", "phone" => "০১৭৩৮২৭৬৩৮৫"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "পীরগঞ্জ", "name" => "সুকমল চন্দ্র রায়", "email" => "udc2digitalcenter@gmail.com", "phone" => "০১৭২৯৬১৬২৯৫"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "পীরগঞ্জ", "name" => "মোছাঃ তানি ইসলাম ইতি", "email" => "taniislameti@gmail.com", "phone" => "০১৭১৩৭০৩১৭৮"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "পীরগঞ্জ", "name" => "অমল চন্দ্র রায়", "email" => "amal999roy@yahoo.com", "phone" => "০১৭৩৭৬৪১৯৯৯"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "পীরগঞ্জ", "name" => "সুমাইয়া আখতার", "email" => "mstlepu@gmail.com", "phone" => "০১৭৫৫৪৪৪৭৯৪"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "পীরগঞ্জ", "name" => "মোঃ রাসেল", "email" => "raselranan.bsc@gmail.com", "phone" => "০১৭৭৪১০২৬১৬"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "পীরগঞ্জ", "name" => "মোঃ ওমর ফারুক", "email" => "omarfaruk435@gmail.com", "phone" => "০১৭৫১০৮০০০৯"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "পীরগঞ্জ", "name" => "গোলাপ চন্দ্র রায়", "email" => "golaproy93@yahoo.com", "phone" => "০১৭১৫৫৭৭১১৮"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "পীরগঞ্জ", "name" => "মোছাঃ তানজি আক্তার", "email" => "tangiakter1997@yahoo.com", "phone" => "০১৭২৩৫৯৪৯১৯"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "পীরগঞ্জ", "name" => "মোঃ মামুন রশীদ", "email" => "mamunur@yahoo.com", "phone" => "০১৭২২১৫৯৭৮৩"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "পীরগঞ্জ", "name" => "মোছাঃ নাজমা আক্তার", "email" => "hajipurudc@gmail.com", "phone" => "০১৭৩৭৭৯৭০৬৬"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "পীরগঞ্জ", "name" => "মোঃ ওমর ফারুক", "email" => "omorfaruk8no@gmail.com", "phone" => "০১৭৩৭৭৭২৪৮১"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "পীরগঞ্জ", "name" => "প্রবিন চন্দ্র রায়", "email" => "uiscdaulatpur8@gmail.com", "phone" => "০১৭৩৮৭০২৭০৩"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "পীরগঞ্জ", "name" => "মোঃ আসাদুল ইসলাম", "email" => "asadul@gmail.com", "phone" => "০১৭৩৮৬৯৬৮৭৩"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "পীরগঞ্জ", "name" => "মোঃ আনছারুল ইসলাম", "email" => "c2ipoint@gmail.com", "phone" => "০১৭৬১০২৬৫৫৯"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "পীরগঞ্জ", "name" => "মোঃ আনছারুল ইসলাম", "email" => "ansarulislam896@gmail.com", "phone" => "০১৭৪০৩০০৮৯৬"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "পীরগঞ্জ", "name" => "মোঃ মনায়েম হাকিম", "email" => "monayemhakim@gmail.com", "phone" => "০১৭৪০৯৬৩৬৮২"],

			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "রাণীশংকৈল", "name" => "মোঃ মোজাম্মেল হক", "email" => "uischarun1@yahoo.com", "phone" => "০১৭১৩৭১০৩৮৩"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "রাণীশংকৈল", "name" => "রোজিনা আকতার মুক্তা", "email" => "necmorod2@yahoo.com", "phone" => "০১৮১৩৫০৩৫৩৩"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "রাণীশংকৈল", "name" => "মো: আশির উদ্দীন", "email" => "mdasir87@gmail.com", "phone" => "০১৭৩৭৭৯৭২৮৩"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "রাণীশংকৈল", "name" => "মোঃ মুসলিম উদ্দীন", "email" => "muslim05rm@gmail.com", "phone" => "০১৭২৩০১৬২৭৩"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "রাণীশংকৈল", "name" => "মো: ওয়াহিদুজ্জামান নুর", "email" => "wahidujjaman.nur.294@gmail.com", "phone" => "০১৭৩৭৭৯৭২৯৪"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "রাণীশংকৈল", "name" => "মোঃ দারুল ইসলাম", "email" => "mddarulislamudc@gmail.com", "phone" => "০১৭৬৭৫০৭৬০১"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "রাণীশংকৈল", "name" => "মোঃ হান্নান শেখ", "email" => "uisc-hannan@yahoo.com", "phone" => "০১৭৩৮৪১৭৯০৮"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "রাণীশংকৈল", "name" => "মোঃ আব্দুল করিম", "email" => "abdulkarimkd30@gmail.com", "phone" => "০১৭২৫৩০২৪২২"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "রাণীশংকৈল", "name" => "সুজন কুমার রায়", "email" => "uiscrator7@yahoo.com", "phone" => "০১৭১৭৯৫৩৮০৬"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "রাণীশংকৈল", "name" => "প্রদীপ রায়", "email" => "ratorup7@gmail.com", "phone" => "০১৭২২৭৯০৯০৯"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "রাণীশংকৈল", "name" => "ভবেশ চন্দ্র রায়", "email" => "bhabesh22@yahoo.com", "phone" => "০১৭৩৭৫৮৯২৪৯"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "রাণীশংকৈল", "name" => "আলন রানী রায়", "email" => "nonduarup8@gmail.com", "phone" => "০১৭৫০৫১৪২৯৩"],

			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "হরিপুর", "name" => "মোঃ জামাল উদ্দীন", "email" => "gadurauisc@yahoo.com", "phone" => "০১৭১৭৯৭৭৪৮৬"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "হরিপুর", "name" => "মোঃ জুলফিকার আলী", "email" => "jeweljadurani@gmail.com", "phone" => "০১৭৩৭৪২৬৯৮৯"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "হরিপুর", "name" => "মোঃ রবিউল আউয়াল", "email" => "mras7263@gmail.com", "phone" => "০১৭৩৩২৮২৮৭৯"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "হরিপুর", "name" => "অজয় কুমার রায়", "email" => "uiscbakua@yahoo.com", "phone" => "০১৭৩৭৮৯১২১৩"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "হরিপুর", "name" => "মোঃ মামুনুর রশিদ", "email" => "mamurr161@gmail.com", "phone" => "০১৯১২৫৯৯৯৫০"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "হরিপুর", "name" => "মোঃ আজিজুর রহমান", "email" => "azizmukul@gmail.com", "phone" => "০১৭৪৪৩৩৩০৭৬"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "হরিপুর", "name" => "মোছাঃ জাকিয়া সুলতানা", "email" => "rifatict18@gmail.com", "phone" => "০১৭২২৮২২২৭১"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "হরিপুর", "name" => "মোঃ গোলাম মোস্তফা", "email" => "golammostofa92@gmail.com", "phone" => "০১৭৩৯৩৯৯০৬৬"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "হরিপুর", "name" => "মোঃ শওকত আলী প্রধান", "email" => "shawkot5@gmail.com", "phone" => "০১৭৩৫৫৬২৪৩১"],
			["division" => "রংপুর", "dist" => "ঠাকুরগাঁও", "sub_dist" => "হরিপুর", "name" => "মোছাঃ ফরিদা পারভীন", "email" => "shawkotprodhan1@gmail.com", "phone" => "০১৭২৯৯৯৩৪৮০"]
		];

		$trans	= ["১"=>"1", "২"=>"2", "৩"=>"3", "৪"=>"4", "৫"=>"5", "৬"=>"6", "৭"=>"7", "৮"=>"8", "৯"=>"9", "০"=>"0", "o"=>"0", "O"=>"0"];

		for ($i=0; $i < count($data); $i++) {
			$list['division'] 		= $data[$i]['division'];
			$list['dist'] 			= $data[$i]['dist'];
			$list['sub_dist'] 		= $data[$i]['sub_dist'];
			$list['name'] 			= $data[$i]['name'];
			$list['phone'] 			= strtr($data[$i]['phone'], $trans);
			$list['email'] 			= $data[$i]['email'];
			$list['created_at'] 	= \Carbon\Carbon::now();

			//My_model::insert('entrepreneurs_list', $list);
		}
			return "It's Completed";
   }
}
