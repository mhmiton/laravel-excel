<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\My_model;
use Excel;

class My_excel extends Controller
{
   public function index()
   {
   		$data['list'] = My_model::get_all_row('entrepreneurs_list', [], 'id,desc', '');
   		return view('main', $data);
   }

   public function makeXLSX()
   {
   		$list = My_model::get_all_row('entrepreneurs_list', [], '', '');
   		// $data = json_decode( json_encode($list), true);
   		$data[] = ["Division", "District", "Sub-District", "Name", "Phone", "Email"];
   		foreach($list as $v)
   		{
	   		$data[] = [
	   			"Division" 		=> $v->division,
	   			"District" 		=> $v->dist,
	   			"Sub-District" 	=> $v->sub_dist,
	   			"Name" 			=> $v->name,
	   			"Phone" 		=> $v->phone,
	   			"Email" 		=> $v->email,
	   		];
	   	}

		Excel::create('Entrepreneurs-List', function($excel) use ($data){
			$excel->setTitle('Entrepreneurs List');

			$excel->sheet('Entrepreneurs-List', function($sheet) use ($data){
				$sheet->fromArray($data, null, 'A1', false, false);
			});
		})->download('xlsx');
   }

   public function send()
   {
   		return "Under Construction";
   }
}
