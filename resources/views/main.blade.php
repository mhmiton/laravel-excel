<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>mySql To Excel Use Laravel | Mehediul Hassan Miton</title>
  </head>
  <body style="margin: 30px;">
    
    <div class="container">
      <div class="col-md-12">
        <div class="col-md-12 text-right" style="margin-bottom: 10px;">
          <a href="{{route('excel.download')}}" class="btn btn-success pull-right">Download</a>
          <a href="{{route('excel.send')}}" class="btn btn-success pull-right">Send</a>
        </div>
        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th scope="col">SL</th>
              <th scope="col">Division</th>
              <th scope="col">District</th>
              <th scope="col">Sub-District</th>
              <th scope="col">Name</th>
              <th scope="col">Phone</th>
              <th scope="col">Email</th>
            </tr>
          </thead>
          <tbody>
            @foreach($list as $key => $v)
              <tr>
                <th scope="row">{{ $key+1 }}</th>
                <td>{{$v->division}}</td>
                <td>{{$v->dist}}</td>
                <td>{{$v->sub_dist}}</td>
                <td>{{$v->name}}</td>
                <td>{{$v->phone}}</td>
                <td>{{$v->email}}</td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>